import models from '../index'
import {
  updateOrderAfterCreate,
  decreaseOrderAfterDeleted,
  updateRelatedOrder
} from './common'
export const getLargestOrderInFeature = async (featureId) => {
  const totalProduct = await models.FeatureProduct.count({
    where: {
      isDeleted: false,
      featureId: featureId
    }
  })

  return totalProduct
}

export const reOrderFeatureProduct = async (featureProduct) => {
  try {
    const whereClause = {
      isDeleted: false,
      featureId: featureProduct.featureId || null
    }

    await updateOrderAfterCreate({
      tableName: 'FeatureProduct',
      createdId: featureProduct.id,
      currentOrder: featureProduct.order,
      baseWhereClause: whereClause
    })
  } catch (error) {
    console.log('reOrderCategory......', error)
  }
}

export const updateOrderFeatureProduct = async ({
  featureProduct,
  previousOrder
}) => {
  const baseWhereClause = {
    isDeleted: false,
    featureId: featureProduct.featureId || null
  }

  await updateRelatedOrder({
    currentOrder: featureProduct.order,
    previousOrder: previousOrder,
    tableName: 'FeatureProduct',
    updatedId: featureProduct.id,
    baseWhereClause: baseWhereClause
  })
}

export const updateOrderDelete = async (featureProduct) => {
  const baseWhereClause = {
    isDeleted: false,
    featureId: featureProduct.featureId || null
  }
  await decreaseOrderAfterDeleted({
    tableName: 'FeatureProduct',
    currentOrder: featureProduct.order,
    deletedId: featureProduct.id,
    baseWhereClause: baseWhereClause
  })
}

export const countProductInFeature = async (featureId) => {
  return await models.FeatureProduct.count({
    where: {
      isDeleted: false,
      featureId: featureId
    }
  })
}
