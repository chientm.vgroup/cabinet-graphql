import models from '../index'
import {
  decreaseOrderAfterDeleted,
  updateRelatedOrder,
  updateOrderAfterCreate
} from './common'
export const reOrderFinishList = async (finishList) => {
  try {
    const whereClause = {
      isDeleted: false,
      parentId: finishList.parentId || null
    }

    await updateOrderAfterCreate({
      tableName: 'FinishList',
      currentOrder: finishList.order,
      createdId: finishList.id,
      baseWhereClause: whereClause
    })
  } catch (error) {
    console.log('reOrderFinishList......', error)
  }
}

export const countSubFinishList = async (parentId = null) => {
  return await models.FinishList.count({
    where: {
      isDeleted: false,
      parentId: parentId
    }
  })
}

export const updateOrderFinishList = async ({ finishList, previousOrder }) => {
  const baseWhereClause = {
    isDeleted: false,
    parentId: finishList.parentId
  }

  await updateRelatedOrder({
    currentOrder: finishList.order,
    previousOrder: previousOrder,
    tableName: 'FinishList',
    updatedId: finishList.id,
    baseWhereClause: baseWhereClause
  })
}

export const updateOrderDelete = async (finishList) => {
  const baseWhereClause = {
    isDeleted: false,
    parentId: finishList.parentId
  }
  await decreaseOrderAfterDeleted({
    tableName: 'FinishList',
    currentOrder: finishList.order,
    deletedId: finishList.id,
    baseWhereClause: baseWhereClause
  })
}
