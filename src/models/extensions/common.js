import models from '../index'
import { Op } from 'sequelize'
import sequelize from '../../sequelize'

export const decreaseOrderAfterDeleted = async ({
  tableName,
  baseWhereClause = {},
  currentOrder,
  deletedId
}) => {
  const order = Number(currentOrder || 1)
  // 1. find all record after deleted record
  const instances = await models[tableName].findAll({
    where: {
      ...baseWhereClause,
      id: {
        [Op.not]: deletedId
      },
      order: {
        [Op.gte]: order
      }
    }
  })

  if (instances?.length > 0) {
    // 2. decrease order
    await Promise.all(
      instances.map(async (instance) => {
        let newOrder = Number(instance.order || 1) - 1
        if (newOrder <= 0) {
          newOrder = 1
        }
        await models[tableName].update(
          {
            order: newOrder
          },
          {
            where: {
              id: instance.id
            }
          }
        )
      })
    )
  }
}

export const updateRelatedOrder = async ({
  tableName,
  baseWhereClause,
  currentOrder,
  previousOrder,
  updatedId
}) => {
  const order = Number(currentOrder || 1)

  let decreaseList = []
  let increaseList = []

  const instanceModal = models[tableName]

  // get list smaller
  const smallerInstances = await instanceModal.findAll({
    where: {
      ...baseWhereClause,
      id: {
        [Op.not]: updatedId
      },
      [Op.and]: [
        {
          order: {
            [Op.lte]: order
          }
        },
        {
          order: {
            [Op.gte]: previousOrder
          }
        }
      ]
    },
    order: [
      ['order', 'desc'],
      ['updatedAt', 'desc']
    ]
  })

  if (smallerInstances?.length > 0) {
    const isSmallestOrder = order - smallerInstances.length <= 1

    decreaseList = isSmallestOrder
      ? smallerInstances.slice(0, order - 1)
      : smallerInstances

    increaseList = isSmallestOrder
      ? smallerInstances.slice(order - 1, smallerInstances.length)
      : []
  }

  // get list bigger
  const biggerInstances = await instanceModal.findAll({
    where: {
      ...baseWhereClause,
      id: {
        [Op.notIn]: [
          updatedId,
          ...smallerInstances.map((instance) => instance.id)
        ]
      },
      order: {
        [Op.gte]: order
      }
    },
    order: [
      ['order', 'asc'],
      ['updatedAt', 'desc']
    ]
  })

  increaseList.push(...biggerInstances)

  // 3. update order of list decrease
  if (decreaseList.length > 0) {
    await Promise.all(
      decreaseList.map(async (instance, index) => {
        const newOrder = order - 1 - index
        if (newOrder !== instance.order) {
          await instanceModal.update(
            {
              order: newOrder
            },
            {
              where: {
                id: instance.id
              }
            }
          )
        }
      })
    )
  }

  // 4. update order of list increase
  if (increaseList.length > 0) {
    await Promise.all(
      increaseList.map(async (instance, index) => {
        const newOrder = order + 1 + index
        if (newOrder !== instance.order) {
          await instanceModal.update(
            {
              order: newOrder
            },
            {
              where: {
                id: instance.id
              }
            }
          )
        }
      })
    )
  }
}

export const updateOrderAfterCreate = async ({
  tableName,
  baseWhereClause,
  currentOrder,
  createdId
}) => {
  const instanceModal = models[tableName]

  const order = Number(currentOrder || 1)
  // 1. get min bigger order
  let minBiggerOrder = await instanceModal.min('order', {
    where: {
      ...baseWhereClause,
      id: {
        [Op.not]: createdId
      },
      [Op.or]: [
        {
          order: {
            [Op.gte]: order
          }
        },
        {
          order: 0
        }
      ]
    }
  })

  if (minBiggerOrder >= 0) {
    const biggerOrders = await instanceModal.findAll({
      where: {
        ...baseWhereClause,
        id: {
          [Op.not]: createdId
        },
        [Op.or]: [
          {
            order: {
              [Op.gte]: minBiggerOrder
            }
          },
          {
            order: 0
          }
        ]
      },
      order: [
        ['order', 'asc'],
        ['updatedAt', 'desc']
      ],
      attributes: ['id', 'order', 'name']
    })
    if (biggerOrders?.length > 0) {
      if (minBiggerOrder <= order) {
        minBiggerOrder = order + 1
      }
      await Promise.all(
        biggerOrders.map(async (instance, index) => {
          const newOrder = minBiggerOrder + index
          if (instance.order < newOrder) {
            const dataUpdate = {
              order: newOrder
            }

            await instanceModal.update(dataUpdate, {
              where: {
                id: instance.id
              }
            })
          }
        })
      )
    }
  }
}

export const autoInCrementField = async ({ tableName, field }) => {
  let clause = `SELECT f.${field} FROM ${tableName} as f order by f.${field} desc limit 1`

  const lastOrder = await sequelize.query(clause, {
    type: sequelize.QueryTypes.SELECT
  })

  return lastOrder ? lastOrder + 1 : 1
}
