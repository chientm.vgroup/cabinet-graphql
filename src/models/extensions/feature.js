import models from '../index'
import { FEATURE_TYPE } from '../../constants/enumType'
import {
  decreaseOrderAfterDeleted,
  updateOrderAfterCreate,
  updateRelatedOrder
} from './common'

export const reOrderFeature = async (feature) => {
  try {
    const whereClause = {
      isDeleted: false,
      featureCategoryId: feature.featureCategoryId || null
    }

    await updateOrderAfterCreate({
      tableName: 'Feature',
      createdId: feature.id,
      currentOrder: feature.order,
      baseWhereClause: whereClause
    })
  } catch (error) {
    console.log('reOrderCategory......', error)
  }
}

export const updateOrderFeature = async ({ feature, previousOrder }) => {
  const baseWhereClause = {
    isDeleted: false,
    featureCategoryId: feature.featureCategoryId || null
  }

  await updateRelatedOrder({
    currentOrder: feature.order,
    previousOrder: previousOrder,
    tableName: 'Feature',
    updatedId: feature.id,
    baseWhereClause: baseWhereClause
  })
}

export const updateOrderDelete = async (feature) => {
  const baseWhereClause = {
    isDeleted: false,
    featureCategoryId: feature.featureCategoryId || null
  }
  await decreaseOrderAfterDeleted({
    tableName: 'Feature',
    currentOrder: feature.order,
    deletedId: feature.id,
    baseWhereClause: baseWhereClause
  })
}

export const createFeatureFromCategory = async (featureCategory) => {
  const features = Object.values(FEATURE_TYPE).map((type, index) => ({
    name: `${featureCategory.name} ${type}`,
    featureCategoryId: featureCategory.id,
    type: type,
    isDeleted: false,
    order: index + 1,
    isDeleted: false,
    images: null
  }))

  await models.Feature.bulkCreate(features, {
    individualHooks: true
  })
}

export const countFeatureInCategory = async (categoryId) => {
  return await models.Feature.count({
    where: {
      isDeleted: false,
      featureCategoryId: categoryId
    }
  })
}

export const initFeatureProductValue = ({
  product,
  featureId,
  order,
  relation = {}
}) => {
  return {
    name: product.name,
    description: product.description,
    featureId: featureId,
    isPublished: !!product.isPublished,
    isDeleted: false,
    images: product.images,
    order: order,
    ...relation
  }
}
