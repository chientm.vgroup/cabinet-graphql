import models from "../index";
import {initPropertyValue} from "./product";

export const createPropertyAllProduct = async (propertyDefault) => {
  // 1. create property with product
  const products = await models.Product.findAll({
    where: {
      isDeleted: false,
    },
    order: [
      ['order', 'asc'],
      ['createdAt', 'asc']
    ]
  })
  // 2. create properties
  if(products && products.length > 0){
    const bulkData = products.map((props, index) => {
      const product = props.dataValues
      return initPropertyValue({
        index: index + 1,
        productId: product.id,
        property: {
          ...propertyDefault.dataValues,
          isShowDefault: true,
          propertyDefaultId: propertyDefault.id
        }
      })
    })
    await models.Property.bulkCreate(bulkData, {
      individualHooks: true
    })
  }
}