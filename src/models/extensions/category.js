import models from '../index'
import {
  decreaseOrderAfterDeleted,
  updateRelatedOrder,
  updateOrderAfterCreate
} from './common'
export const reOrderCategory = async (category) => {
  try {
    const whereClause = {
      isDeleted: false,
      parentId: category.parentId || null
    }

    await updateOrderAfterCreate({
      tableName: 'Category',
      currentOrder: category.order,
      createdId: category.id,
      baseWhereClause: whereClause
    })
  } catch (error) {
    console.log('reOrderCategory......', error)
  }
}

export const countSubCategory = async (parentId = null) => {
  return await models.Category.count({
    where: {
      isDeleted: false,
      parentId: parentId
    }
  })
}

export const updateOrderCategory = async ({ category, previousOrder }) => {
  const baseWhereClause = {
    isDeleted: false,
    parentId: category.parentId
  }

  await updateRelatedOrder({
    currentOrder: category.order,
    previousOrder: previousOrder,
    tableName: 'Category',
    updatedId: category.id,
    baseWhereClause: baseWhereClause
  })
}

export const updateOrderDelete = async (category) => {
  const baseWhereClause = {
    isDeleted: false,
    parentId: category.parentId
  }
  await decreaseOrderAfterDeleted({
    tableName: 'Category',
    currentOrder: category.order,
    deletedId: category.id,
    baseWhereClause: baseWhereClause
  })
}
