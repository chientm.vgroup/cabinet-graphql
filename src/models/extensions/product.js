import models from '../index'
import {
  updateOrderAfterCreate,
  updateRelatedOrder,
  decreaseOrderAfterDeleted
} from './common'

export const countProductInCategory = async (categoryId) => {
  return await models.Product.count({
    where: {
      isDeleted: false,
      categoryId: categoryId
    }
  })
}

export const reOrderProduct = async (product) => {
  try {
    const whereClause = {
      isDeleted: false,
      categoryId: product.categoryId || null
    }

    await updateOrderAfterCreate({
      tableName: 'Product',
      createdId: product.id,
      currentOrder: product.order,
      baseWhereClause: whereClause
    })
  } catch (error) {
    console.log('reOrderCategory......', error)
  }
}

export const updateOrderProduct = async ({ product, previousOrder }) => {
  const baseWhereClause = {
    isDeleted: false,
    categoryId: product.categoryId || null
  }

  await updateRelatedOrder({
    currentOrder: product.order,
    previousOrder: previousOrder,
    tableName: 'Product',
    updatedId: product.id,
    baseWhereClause: baseWhereClause
  })
}

export const updateOrderDelete = async (product) => {
  const baseWhereClause = {
    isDeleted: false,
    categoryId: product.categoryId || null
  }
  await decreaseOrderAfterDeleted({
    tableName: 'Product',
    currentOrder: product.order,
    deletedId: product.id,
    baseWhereClause: baseWhereClause
  })
}

export const initPropertyValue = ({ property, index, productId }) => ({
  key: property.key,
  shortDescription: property.shortDescription,
  defaultValue: property.defaultValue,
  order: index,
  productId: productId,
  description: property.description,
  propertyDefaultId: property.propertyDefaultId,
  isShowDefault: !!property.isShowDefault,
  variables: property.variables
})

export const createProductPropertyDefault = async (product) => {
  // 1.get all property detail
  const propertyDefaults = await models.PropertyDefault.findAll({
    where: {
      isDeleted: false
    },
    order: [
      ['order', 'asc'],
      ['createdAt', 'asc']
    ]
  })
  // 2. create property detail
  if (propertyDefaults && propertyDefaults.length > 0) {
    const bulkData = propertyDefaults.map((props, index) => {
      const property = props.dataValues
      return initPropertyValue({
        index: index + 1,
        productId: product.id,
        property: {
          ...property,
          isShowDefault: true,
          propertyDefaultId: property.id
        }
      })
    })
    await models.Property.bulkCreate(bulkData, {
      individualHooks: true
    })
  }
}
