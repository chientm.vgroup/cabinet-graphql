import { Model, DataTypes } from 'sequelize'
import { defaultListArgs } from 'graphql-sequelize'
import { v4 as uuid } from 'uuid'

import {
  reOrderCategory,
  recalculateOrder,
  updateOrderCategory,
  updateOrderDelete
} from './extensions/category'
class Category extends Model {
  static tableName = 'Category'

  static associate(models) {
    // associations can be defined here
    Category.Parent = Category.belongsTo(models.Category, {
      foreignKey: 'parentId',
      constraints: false,
      as: 'parent'
    })

    Category.Children = Category.hasMany(models.Category, {
      foreignKey: 'parentId',
      sourceKey: 'id',
      constraints: false,
      as: 'children'
    })
  }
}

const schema = {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    allowNull: true
  },
  name: {
    type: DataTypes.STRING(1024),
    allowNull: false
  },
  isPublished: {
    type: DataTypes.BOOLEAN,
    defaultValue: true
  },
  isDeleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  parentId: {
    type: DataTypes.UUID
  },
  order: {
    type: DataTypes.INTEGER,
    defaultValue: 0
  }
}

export default (sequelize) => {
  Category.init(schema, {
    sequelize,
    tableName: Category.tableName,
    timestamps: true,
    freezeTableName: true
  })

  Category.beforeCreate(async (category) => {
    category.id = uuid()
    category.createdAt = new Date()
  })

  Category.beforeUpdate(async (category) => {
    category.updatedAt = new Date()
  })

  Category.afterCreate(async (category) => {
    if (category._changed.has('order')) {
      reOrderCategory(category)
    }
  })

  Category.afterUpdate(async (category) => {
    if (category._changed.has('isDeleted')) {
      if (category.isDeleted) {
        updateOrderDelete(category)
      }
    }
    if (category._changed.has('order')) {
      if (!category.isDeleted) {
        const previousOrder = category._previousDataValues?.order
        updateOrderCategory({
          category,
          previousOrder: previousOrder
        })
      }
    }
  })

  return Category
}

defaultListArgs(Category)
