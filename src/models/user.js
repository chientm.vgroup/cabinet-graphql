import { Model, DataTypes } from 'sequelize'
import { defaultListArgs } from 'graphql-sequelize'
import { v4 as uuid } from 'uuid'
import bcrypt from 'bcryptjs'
import { SALT_WORK_FACTOR } from '../constants'

class User extends Model {
  static tableName = 'User'

  static associate(models) {}
}

const schema = {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    allowNull: true
  },
  firstName: {
    type: DataTypes.STRING
  },
  lastName: {
    type: DataTypes.STRING
  },
  phoneNumber: {
    type: DataTypes.STRING,
    unique: true
  },
  email: {
    type: DataTypes.STRING,
    unique: true,
    allowNull: false
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false
  },
  isPublished: {
    type: DataTypes.BOOLEAN,
    defaultValue: true
  },
  isDeleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  }
}

export default (sequelize) => {
  User.init(schema, {
    sequelize,
    tableName: User.tableName,
    timestamps: true,
    freezeTableName: true
  })

  User.beforeCreate(async (user) => {
    user.id = uuid()
    user.createdAt = new Date()
    user.password = await user.generatePasswordHash()
  })

  User.beforeUpdate(async (user) => {
    user.updatedAt = new Date()
    if (user._changed.has('password')) {
      user.password = await user.generatePasswordHash()
    }
  })

  User.prototype.generatePasswordHash = async function () {
    const salt = bcrypt.genSaltSync(SALT_WORK_FACTOR)
    return bcrypt.hashSync(this.password, salt)
  }

  User.prototype.comparePassword = async function (candidatePassword) {
    return bcrypt.compareSync(candidatePassword, this.password)
  }

  return User
}

defaultListArgs(User)
