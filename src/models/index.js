'use strict'

import { readdirSync } from 'fs'
import path, { basename as _basename } from 'path'
import Sequelize from 'sequelize'
import sequelize from '../sequelize'
const basename = _basename(__filename)
const db = {}

readdirSync(__dirname)
  .filter((file) => {
    return (
      file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js'
    )
  })
  .forEach((file) => {
    const filePath = require(path.join(__dirname, file))?.default
    const model = filePath?.(sequelize)
    db[model.name] = model
  })

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db)
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

export default db
