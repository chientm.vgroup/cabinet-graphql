import { Model, DataTypes } from 'sequelize'
import { defaultListArgs } from 'graphql-sequelize'
import { v4 as uuid } from 'uuid'

class FinishListProduct extends Model {
  static tableName = 'FinishListProduct'

  static associate(models) {
    // associations can be defined here

    FinishListProduct.TierTwo = FinishListProduct.belongsTo(
      models.FeatureProduct,
      {
        foreignKey: 'productId',
        targetKey: 'id',
        constraints: false,
        as: 'product'
      }
    )
    // This creates a foreign key called `productId` in the source model (FinishListProduct)
    // which references the `id` field from the target model (FeatureProduct).

    FinishListProduct.TierThree = FinishListProduct.belongsTo(
      models.FinishList,
      {
        foreignKey: 'finishListId',
        constraints: false,
        as: 'finishList'
      }
    )
  }
}

const schema = {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    allowNull: true
  },
  productId: {
    type: DataTypes.UUID,
    allowNull: false
  },
  finishListId: {
    type: DataTypes.UUID,
    allowNull: false
  },
  order: {
    type: DataTypes.INTEGER,
    defaultValue: 0
  }
}

export default (sequelize) => {
  FinishListProduct.init(schema, {
    sequelize,
    tableName: FinishListProduct.tableName,
    timestamps: true,
    freezeTableName: true
  })

  FinishListProduct.beforeCreate(async (finishListProduct) => {
    finishListProduct.id = uuid()
    finishListProduct.createdAt = new Date()
  })

  FinishListProduct.beforeUpdate(async (finishListProduct) => {
    finishListProduct.updatedAt = new Date()
  })

  return FinishListProduct
}

defaultListArgs(FinishListProduct)
