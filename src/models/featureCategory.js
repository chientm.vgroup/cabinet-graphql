import {Model, DataTypes} from 'sequelize'
import {defaultListArgs} from 'graphql-sequelize'
import {v4 as uuid} from 'uuid'
import {createFeatureFromCategory} from './extensions/feature'

class FeatureCategory extends Model {
  static tableName = 'FeatureCategory'

  static associate(models) {
    FeatureCategory.Feature = FeatureCategory.hasMany(
      models.Feature,
      {
        foreignKey: 'featureCategoryId',
        sourceKey: 'id',
        constraints: false,
        as: 'features'
      }
    )
  }
}

const schema = {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    allowNull: true
  },
  name: {
    type: DataTypes.STRING,
    allowNull: true
  },
  description: {
    type: DataTypes.STRING
  },
  isPublished: {
    type: DataTypes.BOOLEAN,
    defaultValue: true
  },
  isDeleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  }
}

export default (sequelize) => {
  FeatureCategory.init(schema, {
    sequelize,
    tableName: FeatureCategory.tableName,
    timestamps: true,
    freezeTableName: true
  })

  FeatureCategory.beforeCreate(async (featureCategory) => {
    featureCategory.id = uuid()
    featureCategory.createdAt = new Date()
  })

  FeatureCategory.beforeUpdate(async (featureCategory) => {
    featureCategory.updatedAt = new Date()
  })

  FeatureCategory.afterCreate(async (featureCategory) => {
    await createFeatureFromCategory(featureCategory)
  })

  return FeatureCategory
}

defaultListArgs(FeatureCategory)
