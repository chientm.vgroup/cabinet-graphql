import { Model, DataTypes } from 'sequelize'
import { defaultListArgs } from 'graphql-sequelize'
import { v4 as uuid } from 'uuid'
import {
  reOrderFinishList,
  updateOrderDelete,
  updateOrderFinishList
} from './extensions/finishList'

class FinishList extends Model {
  static tableName = 'FinishList'

  static associate(models) {
    // associations can be defined here
    FinishList.Parent = FinishList.belongsTo(models.FinishList, {
      foreignKey: 'parentId',
      constraints: false,
      as: 'parent'
    })

    FinishList.Children = FinishList.hasMany(models.FinishList, {
      foreignKey: 'parentId',
      sourceKey: 'id',
      constraints: false,
      as: 'children'
    })
  }
}

const schema = {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    allowNull: true
  },
  name: {
    type: DataTypes.TEXT,
    allowNull: false
  },
  description: {
    type: DataTypes.TEXT
  },
  isPublished: {
    type: DataTypes.BOOLEAN,
    defaultValue: true
  },
  isDeleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  order: {
    type: DataTypes.INTEGER,
    defaultValue: 0
  },
  images: {
    type: DataTypes.STRING
  },
  parentId: {
    type: DataTypes.UUID
  }
}

export default (sequelize) => {
  FinishList.init(schema, {
    sequelize,
    tableName: FinishList.tableName,
    timestamps: true,
    freezeTableName: true
  })

  FinishList.beforeCreate(async (finishList) => {
    finishList.id = uuid()
    finishList.createdAt = new Date()
  })

  FinishList.beforeUpdate(async (finishList) => {
    finishList.updatedAt = new Date()
  })

  FinishList.afterCreate(async (finishList) => {
    if (finishList._changed.has('order')) {
      reOrderFinishList(finishList)
    }
  })

  FinishList.afterUpdate(async (finishList) => {
    if (finishList._changed.has('isDeleted')) {
      if (finishList.isDeleted) {
        updateOrderDelete(finishList)
      }
    }
    if (finishList._changed.has('order')) {
      if (!finishList.isDeleted) {
        const previousOrder = finishList._previousDataValues?.order
        updateOrderFinishList({
          finishList,
          previousOrder: previousOrder
        })
      }
    }
  })

  return FinishList
}

defaultListArgs(FinishList)
