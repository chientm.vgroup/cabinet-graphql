import { Model, DataTypes } from 'sequelize'
import { defaultListArgs } from 'graphql-sequelize'
import { v4 as uuid } from 'uuid'
import { createPropertyAllProduct } from './extensions/propertyDefault'

class PropertyDefault extends Model {
  static tableName = 'PropertyDefault'
}

const schema = {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    allowNull: true
  },
  key: {
    type: DataTypes.STRING
  },
  shortDescription: {
    type: DataTypes.STRING
  },
  defaultValue: {
    type: DataTypes.STRING
  },
  isDeleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  order: {
    type: DataTypes.INTEGER,
    defaultValue: 0
  }
}

export default (sequelize) => {
  PropertyDefault.init(schema, {
    sequelize,
    tableName: PropertyDefault.tableName,
    timestamps: true,
    freezeTableName: true
  })

  PropertyDefault.beforeCreate(async (PropertyDefault) => {
    PropertyDefault.id = uuid()
    PropertyDefault.createdAt = new Date()
  })

  PropertyDefault.beforeUpdate(async (PropertyDefault) => {
    PropertyDefault.updatedAt = new Date()
  })

  PropertyDefault.afterCreate(async (propertyDefault) => {
    await createPropertyAllProduct(propertyDefault)
  })

  return PropertyDefault
}

defaultListArgs(PropertyDefault)
