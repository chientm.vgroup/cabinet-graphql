import { Model, DataTypes } from 'sequelize'
import { defaultListArgs } from 'graphql-sequelize'
import { v4 as uuid } from 'uuid'
import bcrypt from 'bcryptjs'
import { SALT_WORK_FACTOR } from '../constants'

class UserSessionStyle extends Model {
  static tableName = 'UserSessionStyle'

  static associate(models) {

    UserSessionStyle.UserSession = UserSessionStyle.belongsTo(
      models.UserSession,
      {
        foreignKey: 'userSessionId',
        constraints: false,
        as: 'userSession'
      }
    )

    UserSessionStyle.FeatureCategory = UserSessionStyle.belongsTo(models.FeatureCategory, {
      foreignKey: 'featureCategoryId',
      constraints: false,
      as: 'featureCategory'
    })

    UserSessionStyle.Feature = UserSessionStyle.belongsTo(models.Feature, {
      foreignKey: 'featureId',
      constraints: false,
      as: 'feature'
    })

    UserSessionStyle.FeatureProduct = UserSessionStyle.belongsTo(models.FeatureProduct, {
      foreignKey: 'featureProductId',
      constraints: false,
      as: 'featureProduct'
    })

    UserSessionStyle.FinishList = UserSessionStyle.belongsTo(models.FinishList, {
      foreignKey: 'finishListId',
      constraints: false,
      as: 'finishList'
    })

  }
}

const schema = {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    allowNull: true
  },
  userSessionId: {
    type: DataTypes.UUID,
    allowNull: true
  },
  featureCategoryId: {
    type: DataTypes.UUID
  },
  featureId: {
    type: DataTypes.UUID
  },
  featureProductId: {
    type: DataTypes.UUID
  },
  finishListId: {
    type: DataTypes.UUID
  }
}

export default (sequelize) => {
  UserSessionStyle.init(schema, {
    sequelize,
    tableName: UserSessionStyle.tableName,
    timestamps: true,
    freezeTableName: true
  })

  UserSessionStyle.beforeCreate(async (userSessionStyle) => {
    userSessionStyle.id = uuid()
    userSessionStyle.createdAt = new Date()
  })

  UserSessionStyle.beforeUpdate(async (userSessionStyle) => {
    userSessionStyle.updatedAt = new Date()
  })

  return UserSessionStyle
}

defaultListArgs(UserSessionStyle)
