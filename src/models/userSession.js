import { Model, DataTypes } from 'sequelize'
import { defaultListArgs } from 'graphql-sequelize'
import { v4 as uuid } from 'uuid'
import bcrypt from 'bcryptjs'
import { SALT_WORK_FACTOR } from '../constants'

class UserSession extends Model {
  static tableName = 'UserSession'

  static associate(models) {

    UserSession.User = UserSession.belongsTo(models.User, {
      foreignKey: 'userId',
      constraints: false,
      as: 'user'
    })

    UserSession.UserSessionStyles = UserSession.hasMany(models.UserSessionStyle, {
      foreignKey: 'userSessionId',
      sourceKey: 'id',
      constraints: false,
      as: 'userSessionStyles'
    })

    UserSession.UserSessionJobs = UserSession.hasMany(models.UserSessionJob, {
      foreignKey: 'userSessionId',
      sourceKey: 'id',
      constraints: false,
      as: 'userSessionJobs'
    })
  }
}

const schema = {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    allowNull: true
  },
  userId: {
    type: DataTypes.UUID,
    allowNull: true
  },
  code: {
    type: DataTypes.STRING
  },
  firstName: {
    type: DataTypes.STRING
  },
  lastName: {
    type: DataTypes.STRING
  },
  phoneNumber: {
    type: DataTypes.STRING
  },
  email: {
    type: DataTypes.STRING
  },
  address: {
    type: DataTypes.STRING
  },
  activeStyle: {
    type: DataTypes.UUID
  },
  isCollageStyle: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  isPublished: {
    type: DataTypes.BOOLEAN,
    defaultValue: true
  },
  isDeleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  order: {
    type: DataTypes.INTEGER,
    defaultValue: 0
  }
}

export default (sequelize) => {
  UserSession.init(schema, {
    sequelize,
    tableName: UserSession.tableName,
    timestamps: true,
    freezeTableName: true
  })

  UserSession.beforeCreate(async (userSession) => {
    userSession.id = uuid()
    userSession.createdAt = new Date()
  })

  UserSession.beforeUpdate(async (userSession) => {
    userSession.updatedAt = new Date()
  })

  return UserSession
}

defaultListArgs(UserSession)
