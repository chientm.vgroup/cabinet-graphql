import { Model, DataTypes } from 'sequelize'
import { defaultListArgs } from 'graphql-sequelize'
import { v4 as uuid } from 'uuid'
import { FEATURE_TYPE } from '../constants/enumType'
import {
  reOrderFeature,
  updateOrderDelete,
  updateOrderFeature
} from './extensions/feature'

class Feature extends Model {
  static tableName = 'Feature'

  static associate(models) {
    Feature.FeatureCategory = Feature.belongsTo(models.FeatureCategory, {
      foreignKey: 'featureCategoryId',
      constraints: false,
      as: 'featureCategory'
    })
  }
}

const schema = {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    allowNull: true
  },
  name: {
    type: DataTypes.STRING,
    allowNull: true
  },
  description: {
    type: DataTypes.STRING
  },
  featureCategoryId: {
    type: DataTypes.UUID,
    allowNull: true
  },
  type: {
    type: DataTypes.ENUM(...Object.values(FEATURE_TYPE)),
    allowNull: true
  },
  images: {
    type: DataTypes.STRING
  },
  isPublished: {
    type: DataTypes.BOOLEAN,
    defaultValue: true
  },
  isDeleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  order: {
    type: DataTypes.INTEGER,
    defaultValue: 0
  }
}

export default (sequelize) => {
  Feature.init(schema, {
    sequelize,
    tableName: Feature.tableName,
    timestamps: true,
    freezeTableName: true
  })

  Feature.beforeCreate(async (feature) => {
    feature.id = uuid()
    feature.createdAt = new Date()
  })

  Feature.beforeUpdate(async (feature) => {
    feature.updatedAt = new Date()
  })

  Feature.afterCreate(async (feature) => {
    if (feature._changed.has('order')) {
      reOrderFeature(feature)
    }
  })

  Feature.afterUpdate(async (feature) => {
    if (feature._changed.has('order')) {
      const previousOrder = feature._previousDataValues?.order
      if (!feature.isDeleted) {
        await updateOrderFeature({
          feature,
          previousOrder
        })
      }
    }

    if (feature._changed.has('isDeleted')) {
      if (feature.isDeleted) {
        await updateOrderDelete(feature)
      }
    }
  })

  return Feature
}

defaultListArgs(Feature)
