import { Model, DataTypes } from 'sequelize'
import { defaultListArgs } from 'graphql-sequelize'
import { v4 as uuid } from 'uuid'

import {
  reOrderProduct,
  updateOrderProduct,
  updateOrderDelete,
  createProductPropertyDefault
} from './extensions/product'

class Product extends Model {
  static tableName = 'Product'

  static associate(models) {
    // associations can be defined here
    Product.Category = Product.belongsTo(models.Category, {
      foreignKey: 'categoryId',
      constraints: false,
      as: 'category'
    })

    Product.Property = Product.hasMany(models.Property, {
      foreignKey: 'productId',
      sourceKey: 'id',
      constraints: false,
      as: 'properties'
    })
  }
}

const schema = {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    allowNull: true
  },
  name: {
    type: DataTypes.STRING
  },
  categoryId: {
    type: DataTypes.UUID,
    allowNull: false
  },
  isPublished: {
    type: DataTypes.BOOLEAN,
    defaultValue: true
  },
  isDeleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  order: {
    type: DataTypes.INTEGER,
    defaultValue: 0
  },
  images: {
    type: DataTypes.STRING
    // get() {
    //   return this.getDataValue('images').split(';')
    // },
    // set(val) {
    //   this.setDataValue('images', val.join(';'))
    // }
  }
}

export default (sequelize) => {
  Product.init(schema, {
    sequelize,
    tableName: Product.tableName,
    timestamps: true,
    freezeTableName: true
  })

  Product.beforeCreate(async (Product) => {
    Product.id = uuid()
    Product.createdAt = new Date()
  })

  Product.beforeUpdate(async (Product) => {
    Product.updatedAt = new Date()
  })

  Product.afterCreate(async (product) => {
    // 1. reOrder product
    if (product._changed.has('order')) {
      reOrderProduct(product)
    }
    // 2. create property default
    createProductPropertyDefault(product)
  })

  Product.afterUpdate(async (product) => {
    if (product._changed.has('order')) {
      if (!product.isDeleted) {
        const previousOrder = product._previousDataValues?.order
        await updateOrderProduct({
          product,
          previousOrder: previousOrder
        })
      }
    }

    if (product._changed.has('isDeleted')) {
      if (product.isDeleted) {
        await updateOrderDelete(product)
      }
    }
  })

  return Product
}

defaultListArgs(Product)
