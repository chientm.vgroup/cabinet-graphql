import { Model, DataTypes } from 'sequelize'
import { defaultListArgs } from 'graphql-sequelize'
import { v4 as uuid } from 'uuid'
import bcrypt from 'bcryptjs'
import { SALT_WORK_FACTOR } from '../constants'

class UserSessionJob extends Model {
  static tableName = 'UserSessionJob'

  static associate(models) {

    UserSessionJob.UserSession = UserSessionJob.belongsTo(
      models.UserSession,
      {
        foreignKey: 'userSessionId',
        constraints: false,
        as: 'userSession'
      }
    )

    UserSessionJob.Product = UserSessionJob.belongsTo(
      models.Product,
      {
        foreignKey: 'productId',
        targetKey: 'id',
        constraints: false,
        as: 'product'
      }
    )

  }
}

const schema = {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    allowNull: true
  },
  userSessionId: {
    type: DataTypes.UUID,
    allowNull: true
  },
  productId: {
    type: DataTypes.UUID,
    allowNull: true
  },
  propertyValues: {
    type: DataTypes.STRING,
    allowNull: true
  }
}

export default (sequelize) => {
  UserSessionJob.init(schema, {
    sequelize,
    tableName: UserSessionJob.tableName,
    timestamps: true,
    freezeTableName: true
  })

  UserSessionJob.beforeCreate(async (userSessionJob) => {
    userSessionJob.id = uuid()
    userSessionJob.createdAt = new Date()
  })

  UserSessionJob.beforeUpdate(async (userSessionJob) => {
    userSessionJob.updatedAt = new Date()
  })

  return UserSessionJob
}

defaultListArgs(UserSessionJob)
