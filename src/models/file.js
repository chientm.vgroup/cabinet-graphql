import { Model, DataTypes } from 'sequelize'
import { defaultListArgs } from 'graphql-sequelize'
import { v4 as uuid } from 'uuid'

class File extends Model {
  static tableName = 'File'

  static associate(models) {}
}

const schema = {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    allowNull: true
  },
  filename: {
    type: DataTypes.TEXT,
    allowNull: false
  },
  mimeType: {
    type: DataTypes.TEXT,
    allowNull: false
  },
  fileExtension: {
    type: DataTypes.STRING
  },
  encoding: {
    type: DataTypes.STRING
  }
}

export default (sequelize) => {
  File.init(schema, {
    sequelize,
    tableName: File.tableName,
    timestamps: true,
    freezeTableName: true
  })

  File.beforeCreate(async (file) => {
    file.id = uuid()
    file.createdAt = new Date()
  })

  File.beforeUpdate(async (file) => {
    file.updatedAt = new Date()
  })

  return File
}

defaultListArgs(File)
