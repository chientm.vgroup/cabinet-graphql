import { Model, DataTypes } from 'sequelize'
import { defaultListArgs } from 'graphql-sequelize'
import { v4 as uuid } from 'uuid'
import {
  reOrderFeatureProduct,
  updateOrderDelete,
  updateOrderFeatureProduct
} from './extensions/featureProduct'

class FeatureProduct extends Model {
  static tableName = 'FeatureProduct'

  static associate(models) {
    FeatureProduct.Feature = FeatureProduct.belongsTo(models.Feature, {
      foreignKey: 'featureId',
      constraints: false,
      as: 'feature'
    })

    FeatureProduct.TierThree = FeatureProduct.hasMany(
      models.FinishListProduct,
      {
        foreignKey: 'productId',
        sourceKey: 'id',
        constraints: false,
        as: 'tierThree'
      }
    )
  }
}

const schema = {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    allowNull: true
  },
  name: {
    type: DataTypes.STRING
  },
  description: {
    type: DataTypes.STRING
  },
  featureId: {
    type: DataTypes.UUID,
    allowNull: false
  },
  isPublished: {
    type: DataTypes.BOOLEAN,
    defaultValue: true
  },
  isDeleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  images: {
    type: DataTypes.STRING
  },
  order: {
    type: DataTypes.INTEGER,
    defaultValue: 0
  }
}

export default (sequelize) => {
  FeatureProduct.init(schema, {
    sequelize,
    tableName: FeatureProduct.tableName,
    timestamps: true,
    freezeTableName: true
  })

  FeatureProduct.beforeCreate(async (featureProduct) => {
    featureProduct.id = uuid()
    featureProduct.createdAt = new Date()
  })

  FeatureProduct.beforeUpdate(async (featureProduct) => {
    featureProduct.updatedAt = new Date()
  })

  FeatureProduct.afterCreate(async (featureProduct) => {
    if (featureProduct._changed.has('order')) {
      reOrderFeatureProduct(featureProduct)
    }
  })

  FeatureProduct.afterUpdate(async (featureProduct) => {
    if (featureProduct._changed.has('order')) {
      const previousOrder = featureProduct._previousDataValues?.order
      if (!featureProduct.isDeleted) {
        await updateOrderFeatureProduct({
          featureProduct,
          previousOrder
        })
      }
    }

    if (featureProduct._changed.has('isDeleted')) {
      if (featureProduct.isDeleted) {
        await updateOrderDelete(featureProduct)
      }
    }
  })

  return FeatureProduct
}

defaultListArgs(FeatureProduct)
