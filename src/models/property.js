import { Model, DataTypes } from 'sequelize'
import { defaultListArgs } from 'graphql-sequelize'
import { v4 as uuid } from 'uuid'

class Property extends Model {
  static tableName = 'Property'

  static associate(models) {
    Property.Category = Property.belongsTo(models.Product, {
      foreignKey: 'productId',
      constraints: false,
      as: 'Prod_Cat'
    })
  }
}

const schema = {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    allowNull: true
  },
  productId: {
    type: DataTypes.UUID,
    allowNull: true
  },
  propertyDefaultId: {
    type: DataTypes.UUID,
  },
  key: {
    type: DataTypes.STRING
  },
  shortDescription: {
    type: DataTypes.STRING
  },
  description: {
    type: DataTypes.STRING
  },
  defaultValue: {
    type: DataTypes.STRING
  },
  isPublished: {
    type: DataTypes.BOOLEAN,
    defaultValue: true
  },
  isShowDefault: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  isDeleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  order: {
    type: DataTypes.INTEGER,
    defaultValue: 0
  },
  variables: {
    type: DataTypes.TEXT
  }
}

export default (sequelize) => {
  Property.init(schema, {
    sequelize,
    tableName: Property.tableName,
    timestamps: true,
    freezeTableName: true
  })

  Property.beforeCreate(async (Property) => {
    Property.id = uuid()
    Property.createdAt = new Date()
  })

  Property.beforeUpdate(async (Property) => {
    Property.updatedAt = new Date()
  })

  return Property
}

defaultListArgs(Property)
