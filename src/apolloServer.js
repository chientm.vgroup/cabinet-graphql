import dotenv from 'dotenv'
import { ApolloServer } from 'apollo-server-express'
import { resolver } from 'graphql-sequelize'
import { EXPECTED_OPTIONS_KEY, createContext } from 'dataloader-sequelize'

import schemas from './resolvers'
import models from './models'

dotenv.config()

const IS_DEV_MODE = process.env.NODE_ENV === 'development'

const server = new ApolloServer({
  mocks: false,
  // playground: !!IS_DEV_MODE,
  playground: true,
  //introspection: !!IS_DEV_MODE,
  introspection: true,
  tracing: true,
  schema: schemas,
  path: '/',
  context: (ctx) => {
    try {
      const dataloaderContext = createContext(models.sequelize)

      // Magic swap for key in context
      resolver.contextToOptions = {
        dataloaderContext: [EXPECTED_OPTIONS_KEY]
      }

      return {
        ...ctx,
        models,
        dataloaderContext
      }
    } catch (error) {
      console.log('error.....', error)
    }
  }
})

export default server
