import dotenv from 'dotenv'
import express from 'express'
import bodyParser from 'body-parser'

import server from './apolloServer'
import router from './routes'
import models from './models'
import { UPLOAD_DIRECTORY } from './constants'

var cors = require('cors')

dotenv.config()



const app = express()

app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
app.use(cors())
app.use(`/${UPLOAD_DIRECTORY}`, express.static('uploads'))

app.use(router)

// config limit upload size
app.use(
  bodyParser.json({
    limit: '50mb'
  })
)

app.use(
  bodyParser.urlencoded({
    limit: '50mb',
    extended: true
  })
)

server.applyMiddleware({
  app,
  path: '/',
  cors: true
})

async function start() {
  // Make sure the database tables are up to date
  // gc.getBuckets().then(x => console.log(x))
  await models.sequelize.sync({
    force: false
  })

  const PORT = process.env.PORT || 4000

  app.listen({ port: PORT }, () => {
    console.log(`🚀 Server listening on port ${PORT}`)
  })
}

start()
