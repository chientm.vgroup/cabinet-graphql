import {resolver} from "graphql-sequelize";
import models from "../../models";

export const FeatureCategory = {
  features: (...args) =>
    resolver(models.FeatureCategory.Feature)(...args)
}
