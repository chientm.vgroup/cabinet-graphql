import { resolver } from 'graphql-sequelize'
import models from '../../models'

export const UserSessionJob = {
  userSession: (...args) => resolver(models.UserSessionJob.UserSession)(...args),
  product: (...args) => resolver(models.UserSessionJob.Product)(...args),
}
