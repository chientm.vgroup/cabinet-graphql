import { resolver } from 'graphql-sequelize'
import models from '../../models'

export const Feature = {
  featureCategory: (...args) =>
    resolver(models.Feature.FeatureCategory)(...args)
}
