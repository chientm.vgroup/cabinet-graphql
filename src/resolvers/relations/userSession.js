import { resolver } from 'graphql-sequelize'
import models from '../../models'

export const UserSession = {
  user: (...args) => resolver(models.UserSession.User)(...args),
  userSessionStyles: (...args) => resolver(models.UserSession.UserSessionStyles)(...args),
  userSessionJobs: (...args) => resolver(models.UserSession.UserSessionJobs)(...args)
}
