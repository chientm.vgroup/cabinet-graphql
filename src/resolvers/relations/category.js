import { resolver } from 'graphql-sequelize'
import models from '../../models'

export const Category = {
  parent: (...args) => resolver(models.Category.Parent)(...args),
  children: (...args) => resolver(models.Category.Children)(...args)
}
