import { resolver } from 'graphql-sequelize'
import models from '../../models'

export const FinishList = {
  parent: (...args) => resolver(models.FinishList.Parent)(...args),
  children: (...args) => resolver(models.FinishList.Children)(...args)
}
