import { resolver } from 'graphql-sequelize'
import models from '../../models'

export const FinishListProduct = {
  product: (...args) =>
    resolver(models.FinishListProduct.TierTwo)(...args),
  finishList: (...args) =>
    resolver(models.FinishListProduct.TierThree)(...args)
}
