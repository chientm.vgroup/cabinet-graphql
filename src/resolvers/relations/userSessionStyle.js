import { resolver } from 'graphql-sequelize'
import models from '../../models'

export const UserSessionStyle = {
  userSession: (...args) => resolver(models.UserSessionStyle.UserSession)(...args),
  featureCategory: (...args) => resolver(models.UserSessionStyle.FeatureCategory)(...args),
  feature: (...args) => resolver(models.UserSessionStyle.Feature)(...args),
  featureProduct: (...args) => resolver(models.UserSessionStyle.FeatureProduct)(...args),
  finishList: (...args) => resolver(models.UserSessionStyle.FinishList)(...args),
}
