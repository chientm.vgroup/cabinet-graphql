import { resolver } from 'graphql-sequelize'
import models from '../../models'

export const Product = {
  category: (...args) => resolver(models.Product.Category)(...args),
  properties: (...args) => resolver(models.Product.Property)(...args)
}
