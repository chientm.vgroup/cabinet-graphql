import { resolver } from 'graphql-sequelize'
import models from '../../models'

export const FeatureProduct = {
  feature: (...args) => resolver(models.FeatureProduct.Feature)(...args),
  tierThree: (...args) => resolver(models.FeatureProduct.TierThree)(...args)
}
