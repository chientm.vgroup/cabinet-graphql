import { resolver, createConnectionResolver } from 'graphql-sequelize'

export default {
  userSession: resolver((parent, args, context, info) => context.models.UserSession),

  userSessions: resolver(
    (parent, args, context, info) => context.models.UserSession
  ),

  userSessionsConnection: createConnectionResolver({
    target: (parent, args, context, info) => context.models.UserSession,
    where: (findOptions, args) => args
  }).resolveConnection
}
