import { resolver } from 'graphql-sequelize'

export default {
  user: resolver((parent, args, context, info) => context.models.User)
}
