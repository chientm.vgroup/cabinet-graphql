import { resolver, createConnectionResolver } from 'graphql-sequelize'
import { Op } from 'sequelize'

export default {
  finishList: resolver(
    (parent, args, context, info) => context.models.FinishList
  ),

  finishLists: resolver(
    (parent, args, context, info) => context.models.FinishList
  ),

  finishListsConnection: createConnectionResolver({
    target: (parent, args, context, info) => context.models.FinishList,
    where: (findOptions, args) => args
  }).resolveConnection,

  //customize query
  async searchFinishListByProduct(parent, args, context, info) {
    const { productId, isDeleted, isPublished } = args.where || null

    let finishLists = []

    if (!productId) return finishLists
    // get finish list product
    const finishListProducts = await context.models.FinishListProduct.findAll({
      where: {
        productId: productId
      }
    })
    // check exits finishListProducts
    if (finishListProducts && finishListProducts.length > 0) {
      // get list finishListIds
      const finishListIds = finishListProducts.map((item) => item.finishListId)
      // get finishLists
      finishLists = await context.models.FinishList.findAll({
        where: {
          parentId: finishListIds,
          isPublished: !!isPublished,
          isDeleted: !!isDeleted
        },
        order: [['createdAt', 'asc']]
      })
    }
    return finishLists
  }
}
