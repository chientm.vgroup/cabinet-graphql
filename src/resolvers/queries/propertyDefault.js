import { resolver, createConnectionResolver } from 'graphql-sequelize'

export default {
  propertyDefault: resolver((parent, args, context, info) => context.models.PropertyDefault),

  propertyDefaults: resolver(
    (parent, args, context, info) => context.models.PropertyDefault
  ),

  propertyDefaultsConnection: createConnectionResolver({
    target: (parent, args, context, info) => context.models.PropertyDefault,
    where: (findOptions, args) => args
  }).resolveConnection
}
