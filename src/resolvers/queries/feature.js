import { resolver, createConnectionResolver } from 'graphql-sequelize'

export default {
  feature: resolver((parent, args, context, info) => context.models.Feature),

  features: resolver(
    (parent, args, context, info) => context.models.Feature
  ),

  featuresConnection: createConnectionResolver({
    target: (parent, args, context, info) => context.models.Feature,
    where: (findOptions, args) => args
  }).resolveConnection
}
