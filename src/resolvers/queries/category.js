import { resolver, createConnectionResolver } from 'graphql-sequelize'

export default {
  category: resolver((parent, args, context, info) => context.models.Category),

  categories: resolver(
    (parent, args, context, info) => context.models.Category
  ),

  categoriesConnection: createConnectionResolver({
    target: (parent, args, context, info) => context.models.Category,
    where: (findOptions, args) => args
  }).resolveConnection
}
