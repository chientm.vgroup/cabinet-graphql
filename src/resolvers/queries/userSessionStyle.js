import { resolver, createConnectionResolver } from 'graphql-sequelize'

export default {
  userSessionStyle: resolver((parent, args, context, info) => context.models.UserSessionStyle),

  userSessionStyles: resolver(
    (parent, args, context, info) => context.models.UserSessionStyle
  ),

  userSessionStylesConnection: createConnectionResolver({
    target: (parent, args, context, info) => context.models.UserSessionStyle,
    where: (findOptions, args) => args
  }).resolveConnection
}
