import { resolver, createConnectionResolver } from 'graphql-sequelize'
import { FEATURE_TYPE } from '../../constants/enumType'

export default {
  featureCategory: resolver(
    (parent, args, context, info) => context.models.FeatureCategory
  ),

  featureCategories: resolver(
    (parent, args, context, info) => context.models.FeatureCategory
  ),

  featureCategoriesConnection: createConnectionResolver({
    target: (parent, args, context, info) => context.models.FeatureCategory,
    where: (findOptions, args) => args
  }).resolveConnection,

  async menuFeatureCategories(parent, args, { models }, info) {
    // try {
    const include = [
      {
        model: models.Feature,
        as: 'features',
        required: false,
        where: {
          isDeleted: false,
          isPublished: true,
          type: FEATURE_TYPE.STYLE
        }
      }
    ]
    const featureCategories = await models.FeatureCategory.findAll({
      where: {
        isDeleted: false,
        isPublished: true
      },
      attributes: ['id', 'name'],
      order: [
        ['createdAt', 'asc'],
        [{ model: models.Feature, as: 'features' }, 'order', 'asc']
      ],
      include
    })

    const result = featureCategories.map(async (fc) => {
      const defaultStyle = {
        id: fc.id,
        featureCategoryId: fc.id,
        featureId: null,
        feature: null,
        featureProductId: null,
        featureProduct: null,
        finishListId: null,
        finishList: null
      }
      const defaultFeature = fc.features?.length > 0 ? fc.features[0] : null
      if (defaultFeature) {
        defaultStyle.featureId = defaultFeature.id
        defaultStyle.feature = defaultFeature
        const includeFeature = [
          {
            model: models.FinishListProduct,
            as: 'tierThree',
            required: false,
            include: [
              {
                model: models.FinishList,
                as: 'finishList',
                required: false,
                attributes: ['id'],
                where: {
                  isDeleted: false,
                  isPublished: true
                }
              }
            ]
          }
        ]
        const featureProduct = await models.FeatureProduct.findOne({
          where: {
            featureId: defaultFeature.id,
            isDeleted: false,
            isPublished: true
          },
          order: [
            ['order', 'asc'],
            [
              { model: models.FinishListProduct, as: 'tierThree' },
              'order',
              'asc'
            ]
          ],
          include: includeFeature
        })

        if (featureProduct) {
          defaultStyle.featureProduct = featureProduct
          defaultStyle.featureProductId = featureProduct.id
          const tierThree =
            featureProduct.tierThree?.length > 0
              ? featureProduct.tierThree.map(({ finishListId }) => finishListId)
              : []
          if (tierThree?.length > 0) {
            const finishList = await models.FinishList.findOne({
              where: {
                parentId: tierThree,
                isDeleted: false,
                isPublished: true
              }
              // order: [['order', 'asc']]
            })

            if (finishList) {
              defaultStyle.finishListId = finishList
              defaultStyle.finishList = finishList
            }
          }
        }
      }

      return {
        featureCategory: fc,
        defaultStyle
      }
    })

    return result
    // } catch (error) {
    //   throw new Error(error)
    // }
  }
}
