import { resolver, createConnectionResolver } from 'graphql-sequelize'
import { Op } from 'sequelize'
import _, { maxBy } from 'lodash'
import { MIX_TYPE_DATA } from '../../constants/enumType'

const getListMixTypes = async (context, parentCategoryId = null) => {
  const categoryWhereClause = {
    parentId: parentCategoryId || null,
    isDeleted: false
  }
  const categories = await context.models.Category.findAll({
    where: categoryWhereClause,
    order: [['order', 'ASC']]
  })
  if (!categories || categories.length === 0) {
    return {
      nextOrder: 1,
      list: []
    }
  }

  const maxOrder = maxBy(categories, (category) => category.order)

  const nextOrderCategory = Number(maxOrder?.order ?? 1) + 1

  let mixTypes = []
  await Promise.all(
    categories.map(async (category, index) => {
      const categoryId = category.id
      const catObj = {
        id: categoryId,
        name: category.name,
        isPublished: !!category.isPublished,
        type: MIX_TYPE_DATA.CATEGORY,
        order: category.order,
        children: []
      }

      // get children category
      const recursiveChildren = await getListMixTypes(context, categoryId)
      let children = recursiveChildren.list || []

      // get products by category
      const productWhereClause = {
        categoryId: categoryId,
        isDeleted: false
      }
      const products = await context.models.Product.findAll({
        where: productWhereClause,
        order: [['order', 'ASC']]
        // include: [
        //   {
        //     model: context.models.Property,
        //     as: 'properties',
        //     where: {
        //       isDeleted: false
        //     }
        //   }
        // ]
      })

      const maxOrderProduct = maxBy(products, (product) => product.order)

      const nextOrderProduct = Number(maxOrderProduct?.order ?? 0) + 1

      if (products?.length > 0) {
        products.forEach((product, currentIndexProd) => {
          const indexProduct = currentIndexProd + 1
          const prodObj = {
            id: product.id,
            name: product.name,
            isPublished: !!product.isPublished,
            type: MIX_TYPE_DATA.PRODUCT,
            order: product.order,
            thumbnail:
              product.images?.length > 0 ? product.images.split(';')[0] : null
            // nextOrder: nextOrderProduct
            // children: product?.properties.map((property, indexProp) => {
            //   return {
            //     ...property.get({ plain: true }),
            //     name: property.key,
            //     isDeleted: property.isPublished,
            //     type: MIX_TYPE_DATA.PROPERTY
            //   }
            // })
          }

          children.push(prodObj)
        })
      }

      catObj.children = children || []
      catObj.nextOrder = parentCategoryId
        ? nextOrderProduct
        : recursiveChildren.nextOrder

      mixTypes.push(catObj)
    })
  )

  return {
    nextOrder: nextOrderCategory,
    list: _.sortBy(mixTypes, ['order'])
  }
}

export default {
  product: resolver((parent, args, context, info) => context.models.Product),

  products: resolver((parent, args, context, info) => context.models.Product),

  productsConnection: createConnectionResolver({
    target: (parent, args, context, info) => context.models.Product,
    where: (findOptions, args) => args
  }).resolveConnection,

  async listMixTypes(parent, args, context, info) {
    const mixTypes = await getListMixTypes(context)
    return mixTypes.list
  },

  async searchProductByCategory(parent, args, context, info) {
    const { categoryId } = args.where || {}
    const categories = await context.models.Category.findAll({
      where: {
        [Op.or]: [{ id: categoryId }, { parentId: categoryId }],
        isPublished: true,
        isDeleted: false
      },
      order: [['order', 'asc']]
    })
    if (categories.length === 0) return []
    const listCategoryIds = categories.map((item) => {
      return item.id
    })

    const products = await context.models.Product.findAll({
      where: {
        categoryId: {
          [Op.in]: listCategoryIds
        },
        isPublished: true,
        isDeleted: false
      }
    })

    const searchProducts = []
    categories.map((item) => {
      if (item.parentId) {
        const productByCategory =
          products.length > 0
            ? products.filter((product) => product.categoryId === item.id)
            : []
        searchProducts.push({
          categoryId: item.id,
          categoryName: item.name,
          products: _.sortBy(productByCategory, ['order'])
        })
      }
    })
    return searchProducts
  }
}
