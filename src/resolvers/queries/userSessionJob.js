import { resolver, createConnectionResolver } from 'graphql-sequelize'

export default {
  userSessionJob: resolver((parent, args, context, info) => context.models.UserSessionJob),

  userSessionJobs: resolver(
    (parent, args, context, info) => context.models.UserSessionJob
  ),

  userSessionJobsConnection: createConnectionResolver({
    target: (parent, args, context, info) => context.models.UserSessionJob,
    where: (findOptions, args) => args
  }).resolveConnection
}
