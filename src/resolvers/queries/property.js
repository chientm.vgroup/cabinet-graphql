import { resolver, createConnectionResolver } from 'graphql-sequelize'

export default {
  property: resolver((parent, args, context, info) => context.models.Property),

  properties: resolver(
    (parent, args, context, info) => context.models.Property
  ),

  propertiesConnection: createConnectionResolver({
    target: (parent, args, context, info) => context.models.Property,
    where: (findOptions, args) => args
  }).resolveConnection
}
