import { resolver, createConnectionResolver } from 'graphql-sequelize'

export default {
  finishListProduct: resolver((parent, args, context, info) => context.models.FinishListProduct),

  finishListProducts: resolver(
    (parent, args, context, info) => context.models.FinishListProduct
  ),

  finishListProductsConnection: createConnectionResolver({
    target: (parent, args, context, info) => context.models.FinishListProduct,
    where: (findOptions, args) => args
  }).resolveConnection
}
