import { resolver, createConnectionResolver } from 'graphql-sequelize'

export default {
  featureProduct: resolver((parent, args, context, info) => context.models.FeatureProduct),

  featureProducts: resolver(
    (parent, args, context, info) => context.models.FeatureProduct
  ),

  featureProductsConnection: createConnectionResolver({
    target: (parent, args, context, info) => context.models.FeatureProduct,
    where: (findOptions, args) => args
  }).resolveConnection
}
