import { MIX_TYPE_DATA } from '../../constants/enumType'
import { Op } from 'sequelize'
import {
  countProductInCategory,
  initPropertyValue
} from '../../models/extensions/product'
import { ApolloError } from 'apollo-server'

const handleChangeOrder = async ({
  models,
  tableName,
  id,
  isUp,
  baseWhereClause = {},
  type
}) => {
  const instance = await models[tableName].findOne({
    where: {
      ...baseWhereClause,
      id: id
    }
  })

  if (!instance) {
    return
  }

  const whereClause = {
    ...baseWhereClause,
    id: {
      [Op.not]: id
    }
  }

  switch (type) {
    case MIX_TYPE_DATA.CATEGORY:
      whereClause.parentId = instance.parentId || null
      break
    case MIX_TYPE_DATA.PRODUCT:
      whereClause.categoryId = instance.categoryId || null
      break
    case MIX_TYPE_DATA.PROPERTY:
      whereClause.productId = instance.productId || null
      break
  }

  const order = Number(instance.order || 0)
  let sort = 'desc'
  if (isUp) {
    whereClause.order = {
      [Op.lt]: order
    }
  } else {
    whereClause.order = {
      [Op.gt]: order
    }
    sort = 'asc'
  }
  const nearestInstance = await models[tableName].findOne({
    where: whereClause,
    order: [['order', sort]]
  })

  if (!nearestInstance) {
    return
  }

  const nearestOrder = Number(nearestInstance.order || 0)

  await models[tableName].update(
    {
      order: nearestOrder
    },
    {
      where: {
        id: id
      }
    }
  )

  await models[tableName].update(
    {
      order: order
    },
    {
      where: {
        id: nearestInstance.id
      }
    }
  )
}

const updateProperties = async ({
  currentProperties = [],
  productId,
  models
}) => {
  // 1. check if del all properties
  if (currentProperties.length === 0) {
    await models.Property.update(
      {
        isDeleted: true
      },
      {
        where: {
          productId: productId
        }
      }
    )
  } else {
    // 2. get current all properties
    const properties = await models.Property.findAll({
      where: {
        productId: productId,
        isDeleted: false
      }
    })

    if (properties.length === 0) {
      // 3. create all property
      const bulkData = currentProperties.map((props, index) =>
        initPropertyValue({
          index: index + 1,
          productId: productId,
          property: props
        })
      )

      await models.Property.bulkCreate(bulkData, {
        individualHooks: true
      })
    } else {
      // 4. update properties

      // 4.1. get list remove
      const removeProperties = properties.filter(
        ({ id }) => !currentProperties.some((property) => property.id === id)
      )

      if (removeProperties.length > 0) {
        await models.Property.update(
          {
            isDeleted: true
          },
          {
            where: {
              id: removeProperties.map((property) => property.id)
            }
          }
        )
      }

      // 4.2. get list create
      const createProperties = currentProperties.filter(
        (property) => !property.id
      )

      if (createProperties.length > 0) {
        const bulkData = createProperties.map((props) =>
          initPropertyValue({
            index: currentProperties.indexOf(props) + 1,
            productId: productId,
            property: props
          })
        )

        await models.Property.bulkCreate(bulkData, {
          individualHooks: true
        })
      }

      // 4.3. get list update
      const updateProperties = currentProperties.filter(
        (property) => property.id
      )

      if (updateProperties.length > 0) {
        await Promise.all(
          updateProperties.map(
            async (property) =>
              await models.Property.update(
                initPropertyValue({
                  index: currentProperties.indexOf(property) + 1,
                  productId: productId,
                  property: property
                }),
                {
                  where: {
                    id: property.id
                  }
                }
              )
          )
        )
      }
    }
  }
}

export default {
  async createProduct(parent, { data }, context, info) {
    if (data.order) {
      const maxOrder = await countProductInCategory(data.categoryId)
      if (data.order > maxOrder + 1) {
        data.order = maxOrder + 1
      }
    }
    return await context.models.Product.create(data)
  },

  async updateProduct(
    parent,
    { data, where, properties, isUpdateProperty },
    context,
    info
  ) {
    try {
      if (typeof data.isDeleted === 'boolean') {
        const property = await context.models.Property.findOne({
          where: {
            productId: where.id,
            isDeleted: false
          }
        })

        if (property) {
          return new ApolloError(
            'There are properties in this product. Cannot delete product.'
          )
        }
      }

      const product = await context.models.Product.findOne({
        where: where
      })

      if (data.order) {
        const maxOrder = await countProductInCategory(product.categoryId)
        if (data.order > maxOrder) {
          data.order = maxOrder
        }
      }

      await context.models.Product.update(data, {
        where: where,
        individualHooks: true
      })

      // update properties
      if (isUpdateProperty && product) {
        await updateProperties({
          models: context.models,
          productId: product.id,
          currentProperties: properties
        })
      }

      return product
    } catch (error) {
      throw new Error(error)
    }
  },

  async upDownMixTypeOrder(parent, { where, data }, context) {
    switch (data.type) {
      case MIX_TYPE_DATA.CATEGORY: {
        handleChangeOrder({
          models: context.models,
          tableName: 'Category',
          id: where.id,
          isUp: data.isUp,
          baseWhereClause: {
            isDeleted: false
          },
          type: MIX_TYPE_DATA.CATEGORY
        })
        break
      }
      case MIX_TYPE_DATA.PRODUCT: {
        handleChangeOrder({
          models: context.models,
          tableName: 'Product',
          id: where.id,
          isUp: data.isUp,
          baseWhereClause: {
            isDeleted: false
          },
          type: MIX_TYPE_DATA.PRODUCT
        })
        break
      }
      case MIX_TYPE_DATA.PROPERTY: {
        break
      }
    }
  }
}
