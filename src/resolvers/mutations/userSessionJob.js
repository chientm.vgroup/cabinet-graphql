export default {
  async createUserSessionJob(parent, args, context, info) {
    return await context.models.UserSessionJob.create(args.data)
  },

  async updateUserSessionJob(parent, { data, where }, context, info) {
    try {
      await context.models.UserSessionJob.update(data, {
        where: where,
        individualHooks: true
      })

      return await context.models.UserSessionJob.findOne({
        where: where
      })
    } catch (error) {
      throw new Error(error)
    }
  }
}
