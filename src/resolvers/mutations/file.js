import { v4 as uuid } from 'uuid'
import mime from 'mime'
import path from 'path'
import * as mkdirp from 'mkdirp'
const fs = require('fs')
const parse = require('csv-parse/lib/sync') //include
import { fileExtensions, stringExtensions } from '../../extensions'
import { countSubFinishList } from '../../models/extensions/finishList'
import {
  UPLOAD_DOCUMENT_DIRECTORY,
  UPLOAD_PRODUCT_DIRECTORY
} from '../../constants'

var AdmZip = require('adm-zip')

export default {
  async uploadFile(parent, { file }, context, info) {
    if (file) {
      const { createReadStream, mimetype, encoding } = await file
      const stream = createReadStream()
      const fileExtension = mime.getExtension(mimetype)
      const fileName = uuid()
      const newFileName = `${fileName}.${fileExtension}`

      // save file
      const fileStored = await fileExtensions.storeUpload({
        stream,
        filename: newFileName,
        uploadDir: UPLOAD_PRODUCT_DIRECTORY
      })

      return {
        id: fileName,
        filename: fileStored.filename,
        mimeType: mimetype,
        fileExtension: fileExtension,
        encoding: encoding
      }
    } else {
      return null
    }
  },

  async uploadZipFile(parent, { file }, context, info) {
    try {
      // read a zip file

      const { createReadStream, mimetype } = await file
      const stream = createReadStream()
      const fileExtension = mime.getExtension(mimetype)
      const newFileName = `${uuid()}.${fileExtension}`

      // save file
      await fileExtensions.storeUpload({
        stream,
        filename: newFileName,
        uploadDir: UPLOAD_DOCUMENT_DIRECTORY
      })

      let finishList = []
      let listImages = []

      const pathFile = `${UPLOAD_DOCUMENT_DIRECTORY}/${newFileName}`

      const fileName = path.basename(pathFile).split('.').slice(0, -1).join('.')

      var zip = new AdmZip(pathFile)
      var zipEntries = zip.getEntries() // an array of ZipEntry records

      let currentOrderFinishList = await countSubFinishList()

      zipEntries.forEach(function (zipEntry) {
        const nameSplit = zipEntry.name.split('.')
        const extensions = nameSplit[nameSplit.length - 1]
        switch (extensions) {
          case 'csv': {
            // read csv file
            const records = parse(zipEntry.getData().toString(), {
              columns: (record) =>
                record.map((header) =>
                  stringExtensions.convertStringToCamelCase(header)
                ),
              skip_empty_lines: true,
              trim: true
            })

            if (records.length > 0) {
              currentOrderFinishList += 1
              finishList.push({
                name: nameSplit.slice(0, nameSplit.length - 1).join('.'),
                order: currentOrderFinishList,
                children: records.map((record, index) => ({
                  ...record,
                  order: index + 1
                }))
              })
            }
            break
          }
          case 'png':
          case 'jpeg':
          case 'pnj': {
            // 1. extract file
            mkdirp.sync(UPLOAD_DOCUMENT_DIRECTORY)
            zip.extractEntryTo(
              zipEntry.entryName,
              `${UPLOAD_DOCUMENT_DIRECTORY}/${fileName}`,
              false,
              true
            )

            // 2. upload file to upload image folder
            const newFileName = `${uuid()}.${extensions}`

            fs.copyFile(
              `${UPLOAD_DOCUMENT_DIRECTORY}/${fileName}/${zipEntry.name}`,
              `${UPLOAD_PRODUCT_DIRECTORY}/${newFileName}`,
              (error) => {
                if (error !== null) {
                  console.log('err......', error)
                }
              }
            )

            listImages.push({
              oldImg: zipEntry.name,
              newImg: newFileName
            })

            break
          }
          default:
            break
        }
      })

      // map image with upload folder
      if (finishList.length > 0) {
        finishList.forEach((data) => {
          const { children } = data
          if (children.length > 0) {
            children.forEach((child) => {
              const img = listImages.find(
                ({ oldImg }) => oldImg === child.image
              )
              if (img) {
                child.image = img.newImg
              } else {
                child.image = null
              }
            })
          }
        })
      }

      await Promise.all(
        finishList.map(async (data) => {
          await context.models.FinishList.create(data, {
            individualHooks: true,
            include: [
              {
                model: context.models.FinishList,
                as: 'children'
              }
            ]
          })
        })
      )

      return true
    } catch (error) {
      console.log('error......', error)
    }
  }
}
