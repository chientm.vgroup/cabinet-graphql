import { ApolloError } from 'apollo-server'
import { countSubCategory } from '../../models/extensions/category'

export default {
  async createCategory(parent, { data }, context, info) {
    // check if parent is sub category
    if (data.parentId) {
      const parent = await context.models.Category.findOne({
        where: { isDeleted: false, id: data.parentId }
      })
      if (parent.parentId) {
        return new ApolloError('Cannot create sub category in sub category.')
      }
    }

    if (data.order) {
      const maxOrder = await countSubCategory(data.parentId)
      if (data.order > maxOrder + 1) {
        data.order = maxOrder + 1
      }
    }
    return await context.models.Category.create(data)
  },

  async updateCategory(parent, { data, where }, context, info) {
    try {
      // check before delete
      if (typeof data.isDeleted === 'boolean') {
        // check exist sub category
        const subCategory = await context.models.Category.findOne({
          where: {
            parentId: where.id,
            isDeleted: false
          }
        })

        if (subCategory) {
          return new ApolloError(
            'There are sub categories. Cannot delete category.'
          )
        }

        // check exist product in cat
        const product = await context.models.Product.findOne({
          where: {
            categoryId: where.id,
            isDeleted: false
          }
        })

        if (product) {
          return new ApolloError(
            'There are products in this category. Cannot delete category.'
          )
        }
      }

      const category = await context.models.Category.findOne({
        where: where
      })

      if (data.order) {
        const maxOrder = await countSubCategory(data.parentId)
        if (data.order > maxOrder) {
          data.order = maxOrder
        }
      }

      await context.models.Category.update(data, {
        where: where,
        individualHooks: true
      })

      return category
    } catch (error) {
      throw new Error(error)
    }
  }
}
