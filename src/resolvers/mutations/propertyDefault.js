export default {
  async createPropertyDefault(parent, args, context, info) {
    return await context.models.PropertyDefault.create(args.data)
  },

  async updatePropertyDefault(parent, { data, where }, context, info) {
    try {
      await context.models.PropertyDefault.update(data, {
        where: where,
        individualHooks: true
      })

      return await context.models.PropertyDefault.findOne({
        where: where
      })
    } catch (error) {
      throw new Error(error)
    }
  }
}
