import { ApolloError } from 'apollo-server'
import { countSubFinishList } from '../../models/extensions/finishList'

export default {
  async createFinishList(parent, { data }, context, info) {
    // check if parent is sub FinishList
    if (data.parentId) {
      const parent = await context.models.FinishList.findOne({
        where: { isDeleted: false, id: data.parentId }
      })
      if (parent.parentId) {
        return new ApolloError('Parent is sub finish list. Cannot create.')
      }
    }

    if (data.order) {
      const maxOrder = await countSubFinishList(data.parentId)
      if (data.order > maxOrder + 1) {
        data.order = maxOrder + 1
      }
    }
    return await context.models.FinishList.create(data)
  },

  async updateFinishList(parent, { data, where }, context, info) {
    try {
      // check before delete
      if (typeof data.isDeleted === 'boolean') {
        // check exist sub FinishList
        const subFinishList = await context.models.FinishList.findOne({
          where: {
            parentId: where.id,
            isDeleted: false
          }
        })

        if (subFinishList) {
          return new ApolloError(
            'There are sub finish list. Cannot delete FinishList.'
          )
        }
      }

      const finishList = await context.models.FinishList.findOne({
        where: where
      })

      if (data.order) {
        const maxOrder = await countSubFinishList(data.parentId)
        if (data.order > maxOrder) {
          data.order = maxOrder
        }
      }

      await context.models.FinishList.update(data, {
        where: where,
        individualHooks: true
      })

      return finishList
    } catch (error) {
      throw new Error(error)
    }
  }
}
