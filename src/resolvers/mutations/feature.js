import { ApolloError } from 'apollo-server'
import { FEATURE_TYPE } from '../../constants/enumType'
import { NOT_FOUND } from '../../constants/error'
import {
  countFeatureInCategory,
  initFeatureProductValue
} from '../../models/extensions/feature'
import { countProductInFeature } from '../../models/extensions/featureProduct'

const initFeatureProductRelation = (finishLists = []) => {
  const relation = {
    tierThree:
      finishLists?.length > 0
        ? finishLists.map((finishList, index) => ({
            finishListId: finishList,
            order: index + 1
          }))
        : []
  }
  return relation
}

const bulkCreateFeatureProducts = async ({
  models,
  createProducts = [],
  featureId = null,
  products = []
}) => {
  const isCurrentOrder = products?.length > 0
  const bulkData = createProducts.map((product, order) =>
    initFeatureProductValue({
      featureId: featureId,
      order: !isCurrentOrder ? order + 1 : products.indexOf(product),
      product: product,
      relation: initFeatureProductRelation(product.finishList)
    })
  )

  await Promise.all(
    bulkData.map(async (data) => {
      await models.FeatureProduct.create(data, {
        include: [
          {
            model: models.FinishListProduct,
            as: 'tierThree'
          }
        ]
      })
    })
  )
}

const updateFeatureProductRelation = async (models, product = {}) => {
  const productId = product.id
  const listFinishListId = product.finishList
  // 1. check if del all relation
  if (listFinishListId?.length === 0) {
    await models.FinishListProduct.destroy({
      where: {
        productId: productId
      }
    })
    return
  }

  // 2. get current relation
  const currentRelation = await models.FinishListProduct.findAll({
    where: {
      productId: productId
    },
    order: [['order', 'asc']]
  })

  // 2.1. if not exist relation
  if (currentRelation?.length === 0) {
    const bulkData = listFinishListId.map((finishListId) => ({
      productId: productId,
      finishListId: finishListId,
      order: listFinishListId.indexOf(finishListId) + 1
    }))

    await models.FinishListProduct.bulkCreate(bulkData, {
      individualHooks: true
    })

    return
  }

  // 2.2. update relation
  // 2.2.1. get list remove
  const removeRelation = currentRelation.filter(
    (relation) =>
      !listFinishListId.some(
        (finishListId) => finishListId === relation.finishListId
      )
  )

  if (removeRelation?.length > 0) {
    await models.FinishListProduct.destroy({
      where: {
        id: removeRelation.map((relation) => relation.id)
      }
    })
  }

  // 2.2.2. get list create
  const createRelation = listFinishListId.filter(
    (id) => !currentRelation.some(({ finishListId }) => finishListId === id)
  )

  if (createRelation?.length > 0) {
    const bulkData = createRelation.map((finishListId) => ({
      productId: productId,
      finishListId: finishListId,
      order: listFinishListId.indexOf(finishListId) + 1
    }))

    await models.FinishListProduct.bulkCreate(bulkData, {
      individualHooks: true
    })
  }

  // 2.2.3. get list update
  const updateRelation = listFinishListId.filter((id, index) => {
    const existRelation = currentRelation.find(
      ({ finishListId }) => finishListId === id
    )

    if (existRelation) {
      return currentRelation.indexOf(existRelation) !== index
    }
    return false
  })

  if (updateRelation.length > 0) {
    await Promise.all(
      updateRelation.map(async (id) => {
        const relation = currentRelation.find(
          ({ finishListId }) => finishListId === id
        )

        if (relation) {
          await models.FinishListProduct.update(
            {
              order: listFinishListId.indexOf(id) + 1
            },
            {
              where: {
                id: relation.id
              }
            }
          )
        }
      })
    )
  }

  return
}

const updateFeatureProducts = async ({ models, featureId, products = [] }) => {
  // 1. check if del all products
  if (products?.length === 0) {
    await models.FeatureProduct.update(
      {
        isDeleted: true
      },
      {
        where: {
          featureId: featureId
        }
      }
    )

    return
  }

  // 2. get current products
  const currentProducts = await models.FeatureProduct.findAll({
    where: {
      isDeleted: false,
      featureId: featureId
    }
  })

  // 2.1. if not exist products, create all
  if (currentProducts?.length === 0) {
    await bulkCreateFeatureProducts({
      models,
      createProducts: products,
      featureId,
      products: []
    })
    return
  }

  // 2.2. update products
  // 2.2.1. get list remove
  const removeProducts = currentProducts.filter(
    ({ id }) => !products.some((product) => product.id === id)
  )

  if (removeProducts.length > 0) {
    await models.FeatureProduct.update(
      {
        isDeleted: true
      },
      {
        where: {
          id: removeProducts.map((product) => product.id)
        }
      }
    )
  }

  // 2.2.2. get list create
  const createProducts = products.filter((product) => !product.id)

  if (createProducts.length > 0) {
    await bulkCreateFeatureProducts({
      models,
      createProducts,
      featureId,
      products
    })
  }

  // 2.2.3. get list update
  const updateProducts = products.filter((product) => product.id)

  if (updateProducts.length > 0) {
    await Promise.all(
      updateProducts.map(async (product) => {
        // 2.2.3.1. update feature product
        await models.FeatureProduct.update(
          initFeatureProductValue({
            order: products.indexOf(product) + 1,
            featureId: featureId,
            product: product
          }),
          {
            where: {
              id: product.id
            }
          }
        )
        // 2.2.3.2. update relation
        await updateFeatureProductRelation(models, product)
      })
    )
  }
  // end
}

const createTierByFeatureCat = async ({
  models,
  featureCategory,
  tierOne,
  tierTwo,
  tierThree
}) => {
  if (tierOne > 0) {
    // 1. tier 1: feature
    // 1.1. init data
    const totalFeature = await countFeatureInCategory(featureCategory.id)
    let dataTierOne = []
    for (let indexTierOne = 0; indexTierOne < tierOne; indexTierOne++) {
      const index = totalFeature + indexTierOne + 1
      const dataFeature = {
        name: `${featureCategory.name} - ${index}`,
        featureCategoryId: featureCategory.id,
        type: FEATURE_TYPE.STYLE,
        isDeleted: false,
        isPublished: true,
        order: index
      }

      dataTierOne.push(dataFeature)
    }

    // 1.2. create
    const features = await models.Feature.bulkCreate(dataTierOne, {
      individualHooks: true
    })

    if (features.length > 0 && tierTwo > 0) {
      // 2. tier 2: feature product
      // 2.1. init data
      await Promise.all(
        features.map(async (feature) => {
          const totalFeatureProduct = await countProductInFeature(feature.id)
          let dataTierTwo = []
          for (let indexTierTwo = 0; indexTierTwo < tierTwo; indexTierTwo++) {
            const index = totalFeatureProduct + indexTierTwo + 1
            const dataFeatureProduct = {
              name: `${feature.name} - ${index}`,
              featureId: feature.id,
              isDeleted: false,
              isPublished: true,
              order: index
            }
            dataTierTwo.push(dataFeatureProduct)
          }

          // 2.2. create
          const featureProducts = await models.FeatureProduct.bulkCreate(
            dataTierTwo,
            {
              individualHooks: true
            }
          )

          if (featureProducts.length > 0 && tierThree > 0) {
            // 3. tier 3
          }
        })
      )
    }
  }
}

export default {
  async createFeature(parent, args, context, info) {
    return await context.models.Feature.create(args.data)
  },

  async updateFeature(
    parent,
    { data, where, featureProducts, isUpdateProduct },
    context,
    info
  ) {
    try {
      // check if is delete feature
      if (data.isDeleted) {
        const featureProducts = await context.models.FeatureProduct.count({
          where: {
            isDeleted: false,
            featureId: where.id
          }
        })

        if (featureProducts > 0) {
          return new ApolloError(
            'There are products in this feature. Cannot delete.'
          )
        }
      }

      const feature = await context.models.Feature.findOne({
        where: where
      })

      if (!feature) {
        return new ApolloError('Not found feature!')
      }

      // check order
      if (data.order) {
        const maxOrder = await countFeatureInCategory(feature.featureCategoryId)
        if (data.order > maxOrder) {
          data.order = maxOrder
        }
      }

      // update
      await context.models.Feature.update(data, {
        where: where,
        individualHooks: true
      })

      // update feature product
      if (isUpdateProduct && featureProducts) {
        await updateFeatureProducts({
          models: context.models,
          featureId: feature.id,
          products: featureProducts
        })
      }

      return await context.models.Feature.findOne({
        where: where
      })
    } catch (error) {
      console.log('error...', error)
      throw new Error(error)
    }
  },

  async createMultiData(
    parent,
    { featureCategoryId, tierOne, tierTwo, tierThree },
    context,
    info
  ) {
    try {
      // tier 2: feature product
      // tier 3: finish list
      if (featureCategoryId) {
        const featureCategory = await context.models.FeatureCategory.findByPk(
          featureCategoryId
        )
        if (!featureCategory || featureCategory.isDeleted) {
          return new ApolloError('Instance Not found', NOT_FOUND)
        }

        await createTierByFeatureCat({
          models: context.models,
          featureCategory,
          tierOne,
          tierTwo,
          tierThree
        })
      } else {
        const featureCategories = await context.models.FeatureCategory.findAll({
          where: {
            isDeleted: false
          }
        })

        await Promise.all(
          featureCategories.map(async (featureCategory) => {
            await createTierByFeatureCat({
              models: context.models,
              featureCategory,
              tierOne,
              tierTwo,
              tierThree
            })
          })
        )
      }
      return true
    } catch (error) {
      throw new Error(error)
    }
  }
}
