export default {
  async createProperty(parent, args, context, info) {
    return await context.models.Property.create(args.data)
  },

  async updateProperty(parent, { data, where }, context, info) {
    try {
      await context.models.Property.update(data, {
        where: where,
        individualHooks: true
      })

      return await context.models.Property.findOne({
        where: where
      })
    } catch (error) {
      throw new Error(error)
    }
  }
}
