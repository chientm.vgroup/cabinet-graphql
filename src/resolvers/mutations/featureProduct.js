import { getLargestOrderInFeature } from '../../models/extensions/featureProduct'
import { ApolloError } from 'apollo-server'
export default {
  async createFeatureProduct(parent, { data }, context, info) {
    try {
      if (data.order) {
        // check order
        const maxOrder = await getLargestOrderInFeature(data.featureId)
        const maxNextOrder = maxOrder ? Number(maxOrder) + 1 : 1
        if (data.order > maxNextOrder) {
          data.order = maxNextOrder
        }
      } else {
        data.order = 1
      }

      // create feature product
      return await context.models.FeatureProduct.create(data)
    } catch (error) {
      throw new Error(error)
    }
  },

  async updateFeatureProduct(parent, { data, where }, context, info) {
    try {
      if (data.order) {
        const maxOrder = await getLargestOrderInFeature(data.featureId)
        const maxNextOrder = maxOrder ?? 1
        if (data.order > maxNextOrder) {
          data.order = maxNextOrder
        }
      }
      await context.models.FeatureProduct.update(data, {
        where: where,
        individualHooks: true
      })

      return await context.models.FeatureProduct.findOne({
        where: where
      })
    } catch (error) {
      throw new Error(error)
    }
  }
}
