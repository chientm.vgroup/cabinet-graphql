import {ApolloError} from "apollo-server";
import {AUTHENTICATE_FAIL} from "../../constants/error";

export default {
  async createUserSession(parent, args, context, info) {
    return await context.models.UserSession.create(args.data,
      {
        include: [
          {
            model: context.models.UserSessionStyle,
            as: 'userSessionStyles'
          },
          {
            model: context.models.UserSessionJob,
            as: 'userSessionJobs'
          }
        ]
      })
  },

  async updateUserSession(parent, {data, where}, context, info) {
    try {
      const {userSessionStyles, userSessionJobs} = data

      const userSession = await context.models.UserSession.findOne({
        where: where
      })

      if (!userSession) return new ApolloError('NOT EXIST USER SESSION')

      await context.models.UserSession.update(data,
        {
          include: [
            {
              model: context.models.UserSessionStyle,
              as: 'userSessionStyles'
            },
            {
              model: context.models.UserSessionJob,
              as: 'userSessionJobs'
            }
          ],
          where: where,
          individualHooks: true
        })

      // await context.models.UserSessionStyle.destroy({
      //   where: {
      //     userSessionId: userSession.id
      //   }
      // })
      //
      // console.log('userSessionStyles..............', userSessionStyles)
      //
      // await context.models.UserSessionStyle.bulkCreate(userSessionStyles, {
      //   individualHooks: true
      // })

      return await context.models.UserSession.findOne({
        where: where
      })
    } catch (error) {
      //console.log('error........', error)
      throw new Error(error)
    }
  }
}
