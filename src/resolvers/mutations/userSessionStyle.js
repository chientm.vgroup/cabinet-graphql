export default {
  async createUserSessionStyle(parent, args, context, info) {
    return await context.models.UserSessionStyle.create(args.data)
  },

  async updateUserSessionStyle(parent, { data, where }, context, info) {
    try {
      await context.models.UserSessionStyle.update(data, {
        where: where,
        individualHooks: true
      })

      return await context.models.UserSessionStyle.findOne({
        where: where
      })
    } catch (error) {
      throw new Error(error)
    }
  }
}
