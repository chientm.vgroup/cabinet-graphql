export default {
  async createUser(parent, args, context, info) {
    return await context.models.User.create(args.data)
  },

  async updateUser(parent, { data, where }, context, info) {
    try {
      await context.models.User.update(data, {
        where: where,
        individualHooks: true
      })

      return await context.models.User.findOne({
        where: where
      })
    } catch (error) {
      throw new Error(error)
    }
  },

  async deleteUser(parent, { where }, context, info) {
    try {
      await context.models.User.delete({
        where: where
      })

      return true
    } catch (error) {
      throw new Error(error)
    }
  }
}
