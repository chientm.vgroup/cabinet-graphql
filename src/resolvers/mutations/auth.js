import { ApolloError } from 'apollo-server'
import { TOKEN_TYPE } from '../../constants/enumType'
import {
  AUTHENTICATE_FAIL,
  INVALID_TOKEN,
  NOT_FOUND
} from '../../constants/error'
import {
  getTokenFromRequest,
  verifyToken,
  generateAuthToken
} from '../../extensions/jwt'

export default {
  async login(parent, { email, password }, context, info) {
    try {
      // check user
      const user = await context.models.User.findOne({
        where: {
          email: email
        }
      })

      if (!user || !(await user.comparePassword(password))) {
        return new ApolloError(
          'Email or password do not match',
          AUTHENTICATE_FAIL
        )
      }

      return generateAuthToken(user, process.env.FRONTEND_DOMAIN)
    } catch (error) {
      throw new Error(error)
    }
  },

  async loginAdmin(parent, { email, password }, context, info) {
    try {
      // check user
      const user = await context.models.User.findOne({
        where: {
          email: email
        }
      })

      if (!user || !(await user.comparePassword(password))) {
        return new ApolloError(
          'Email or password do not match',
          AUTHENTICATE_FAIL
        )
      }

      return generateAuthToken(user, process.env.ADMIN_DOMAIN)
    } catch (error) {
      throw new Error(error)
    }
  },

  async refreshToken(parent, { token }, context, info) {
    try {
      // 1. check token
      const currentToken = getTokenFromRequest(context.req)
      const decodedToken = verifyToken(currentToken)
      const decodeRefresh = verifyToken(token)
      if (
        decodedToken.type !== TOKEN_TYPE.BEARER ||
        decodeRefresh.type !== TOKEN_TYPE.REFRESH ||
        decodedToken.sub !== decodeRefresh.sub
      ) {
        return new ApolloError('Invalid token', INVALID_TOKEN)
      }

      // 2. get user
      const user = await context.models.User.findByPk(decodedToken.sub)

      if (!user) {
        return new ApolloError('User not found.', AUTHENTICATE_FAIL)
      }

      return generateAuthToken(user, decodedToken.aud)
    } catch (error) {
      throw error
    }
  },

  async changePassword(parent, { oldPassword, newPassword }, context, info) {
    try {
      // 1. check token
      const currentToken = getTokenFromRequest(context.req)
      const decodedToken = verifyToken(currentToken)
      if (decodedToken.type !== TOKEN_TYPE.BEARER) {
        return new ApolloError('Invalid token', INVALID_TOKEN)
      }

      // 2. get user
      const user = await context.models.User.findByPk(decodedToken.sub)

      if (!user || !(await user.comparePassword(oldPassword))) {
        return new ApolloError('Password do not match', AUTHENTICATE_FAIL)
      }

      // 3. update user
      await context.models.User.update(
        {
          password: newPassword
        },
        {
          where: {
            id: user.id
          },
          individualHooks: true
        }
      )
      return true
    } catch (error) {
      throw new Error(error)
    }
  },

  async changeUserPassword(parent, { email, password }, context, info) {
    try {
      // 1. check user exist
      const user = await context.models.User.findOne({
        where: {
          email: email
        }
      })

      if (!user) {
        return new ApolloError('User not found', NOT_FOUND)
      }

      await context.models.User.update(
        {
          password: password
        },
        {
          where: {
            id: user.id
          },
          individualHooks: true
        }
      )

      return true
    } catch (error) {
      throw new Error(error)
    }
  }
}
