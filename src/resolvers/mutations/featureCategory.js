export default {
  async createFeatureCategory(parent, args, context, info) {
    return await context.models.FeatureCategory.create(args.data)
  },

  async updateFeatureCategory(parent, { data, where }, context, info) {
    try {
      await context.models.FeatureCategory.update(data, {
        where: where,
        individualHooks: true
      })

      return await context.models.FeatureCategory.findOne({
        where: where
      })
    } catch (error) {
      throw new Error(error)
    }
  }
}
