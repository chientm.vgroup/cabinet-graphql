import path from 'path'
import { loadFilesSync } from '@graphql-tools/load-files'
import { mergeTypeDefs } from '@graphql-tools/merge'
import { makeExecutableSchema } from '@graphql-tools/schema'
import { typeDefs as typeScalar } from 'graphql-scalars'

import Queries from './queries'
import Mutations from './mutations'
import Relations from './relations'

import { objectExtensions } from '../extensions'

const schemaPath = path.join(__dirname, '../schemas/*.graphql')
const enumPath = path.join(__dirname, '../constants/enumType.js')

const enumFile = require(enumPath)

const EnumResolver = objectExtensions.convertPropObjectToCapitalize(enumFile)

const resolvers = {
  ...EnumResolver,
  Query: Queries,
  Mutation: Mutations,
  ...Relations
}

const loadedFiles = loadFilesSync(schemaPath)

const typeDefs = mergeTypeDefs([...typeScalar, ...loadedFiles], { all: true })

// schema
export default makeExecutableSchema({
  typeDefs: typeDefs,
  resolvers: resolvers
})
