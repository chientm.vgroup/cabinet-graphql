import Sequelize from 'sequelize'
const config = require(__dirname + '/config/config.js')

const sequelize = new Sequelize(
  config.database,
  config.username,
  config.password,
  config
)

async function checkAuthorize() {
  try {
    await sequelize.authenticate()
    console.log('connect success.....')
  } catch (error) {
    console.log('error when connect sequelize....', error)
  }
}

checkAuthorize()

export default sequelize
