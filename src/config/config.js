const dotenv = require('dotenv')

dotenv.config()

module.exports = {
  username: process.env.DB_USERNAME || 'root',
  password: process.env.DB_PASSWORD || 'root',
  database: process.env.DB_NAME || 'cabinet',
  host: process.env.DB_HOST || 'localhost',
  dialect: process.env.DB_DIALECT || 'mysql',
  port: process.env.DB_PORT || '3306',
  logging: false,
  sync: { force: false },
  dialectOptions: {
    connectTimeout: 30000
  },
  pool: {
    // If you want to override the options used for the read/write pool you can do so here
    max: 20,
    idle: 30000
  }
}
