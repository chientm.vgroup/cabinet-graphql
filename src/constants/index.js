export const UPLOAD_DIRECTORY = 'uploads'

export const UPLOAD_PRODUCT_DIRECTORY = `${UPLOAD_DIRECTORY}/products`

export const UPLOAD_DOCUMENT_DIRECTORY = `${UPLOAD_DIRECTORY}/documents`

export const SALT_WORK_FACTOR = 10

export const ALGORITHM_TOKEN = 'HS256'

export const TOKEN_EXPIRED_TIME = '10m'
export const REFRESH_TOKEN_EXPIRED_TIME = '15m'
export const TOKEN_EXPIRED = 10 * 60 * 1000
export const REFRESH_AFTER_TIME = 5 * 60 * 1000
