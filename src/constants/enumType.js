export const MIX_TYPE_DATA = {
  CATEGORY: 'category',
  PRODUCT: 'product',
  PROPERTY: 'property'
}

export const FEATURE_TYPE = {
  STYLE: 'Style',
  MATERIAL: 'Material'
}

export const GRANT_TYPE = {
  AUTHORIZATION: 'authorization_code',
  REFRESH_TOKEN: 'refresh_token'
}

export const TOKEN_TYPE = {
  BEARER: 'Bearer',
  REFRESH: 'Refresh'
}
