import express from 'express'
import path from 'path'
import * as mkdirp from 'mkdirp'
const fs = require('fs')
import { UPLOAD_PRODUCT_DIRECTORY } from './constants'
import { resizeImage } from './extensions/images'
import { EXCEPTION, INVALID_FILE } from './constants/error'
const router = express.Router()

const pathFile = path.join(__dirname, '/sample', `sample.zip`)

router.get('/download/sample', async (req, res) => {
  res.download(pathFile)
})

// router.get('/images/:name', async (req, res) => {
//   try {
//     // Extract the query-parameter
//     const imgName = req.params.name
//     const widthString = req.query.w
//     const heightString = req.query.h
//
//     // Parse to integer if possible
//     let width, height
//     if (widthString) {
//       width = parseInt(widthString)
//     }
//     if (heightString) {
//       height = parseInt(heightString)
//     }
//
//     mkdirp.sync(UPLOAD_PRODUCT_DIRECTORY)
//     const filePath = `${UPLOAD_PRODUCT_DIRECTORY}/${imgName}`
//
//     const file = fs.lstatSync(filePath)
//     if (file.isFile()) {
//       resizeImage({
//         path: filePath,
//         height,
//         width
//       }).pipe(res)
//     } else {
//       res.status(400).send({
//         status: 400,
//         code: INVALID_FILE
//       })
//     }
//   } catch (error) {
//     res.status(400).send({
//       status: 400,
//       code: error?.code ?? EXCEPTION
//     })
//   }
// })

export default router
