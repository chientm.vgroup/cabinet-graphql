import { camelCase, startCase } from 'lodash'

export const convertStringToCamelCase = (string) => {
  if (!string) {
    return null
  }

  return camelCase(string)
}

export const convertStringToStartCase = (string) => {
  if (!string) {
    return null
  }

  return startCase(string)
}

export const convertStringToCapitalize = (string) => {
  if (!string) {
    return null
  }

  const stringWithCamel = convertStringToCamelCase(string)
  const stringWithStart = convertStringToStartCase(stringWithCamel)
  if (!stringWithStart) {
    return null
  }

  return stringWithStart.replace(/\s/g, '')
}

export default {
  convertStringToCamelCase,
  convertStringToStartCase,
  convertStringToCapitalize
}
