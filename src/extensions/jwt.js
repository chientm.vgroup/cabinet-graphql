import { ApolloError } from 'apollo-server'
import jwt from 'jsonwebtoken'
import {
  ALGORITHM_TOKEN,
  REFRESH_AFTER_TIME,
  REFRESH_TOKEN_EXPIRED_TIME,
  TOKEN_EXPIRED,
  TOKEN_EXPIRED_TIME
} from '../constants'
import { TOKEN_TYPE } from '../constants/enumType'
import { getNewDate } from './dateTime'

export const APP_SECRET =
  process.env.APP_SECRET || 'APBxxIU5^CsjDNc$oF1r7FW/i%a4gPtL'

export const generateToken = ({
  data = {},
  subject,
  expiresIn = '1h',
  tokenType,
  audience
}) => {
  return jwt.sign(data, APP_SECRET, {
    algorithm: ALGORITHM_TOKEN,
    expiresIn: expiresIn,
    issuer: process.env.UPLOAD_ENDPOINT,
    subject: subject,
    audience: audience,
    header: {
      type: tokenType
    }
  })
}

export const getTokenFromRequest = (req) => {
  let token
  if (req.headers && req.headers.authorization) {
    var parts = req.headers.authorization.split(' ')
    if (parts.length == 2) {
      var scheme = parts[0]
      var credentials = parts[1]

      if (/^Bearer$/i.test(scheme)) {
        token = credentials
      }
    } else {
      throw new ApolloError(
        'Format is Authorization: Bearer [token]',
        'credentials_bad_format'
      )
    }
  }

  if (!token) {
    throw new ApolloError(
      'No authorization token was found',
      'credentials_required'
    )
  }

  return token
}

export const decodeToken = (token) => {
  try {
    return jwt.decode(token, { complete: true }) || {}
  } catch (error) {
    throw new ApolloError('invalid_token', err)
  }
}

export const verifyToken = (token) => {
  try {
    return jwt.verify(token, APP_SECRET)
  } catch (error) {
    throw new Error(error)
  }
}

export const generateAuthToken = async (user, audience) => {
  const expiresIn = Number(getNewDate()) + TOKEN_EXPIRED
  const refreshExpiresIn = expiresIn + REFRESH_AFTER_TIME

  const token = generateToken({
    subject: user.id,
    tokenType: TOKEN_TYPE.BEARER,
    expiresIn: TOKEN_EXPIRED_TIME,
    audience: audience,
    data: {
      user: {
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email
      },
      type: TOKEN_TYPE.BEARER
    }
  })

  const refreshToken = generateToken({
    subject: user.id,
    tokenType: TOKEN_TYPE.REFRESH,
    expiresIn: REFRESH_TOKEN_EXPIRED_TIME,
    audience: audience,
    data: {
      type: TOKEN_TYPE.REFRESH
    }
  })

  return {
    token,
    refreshToken,
    expiresIn,
    refreshExpiresIn
  }
}

export default {
  generateToken,
  getTokenFromRequest,
  decodeToken,
  verifyToken,
  generateAuthToken
}
