import { createWriteStream } from 'fs'
import * as mkdirp from 'mkdirp'

const extract = require('extract-zip')

const storeUpload = async ({ stream, uploadDir, filename }) => {
  mkdirp.sync(uploadDir)
  const filePath = `${uploadDir}/${filename}`
  return new Promise((resolve, reject) =>
    stream
      .pipe(createWriteStream(filePath))
      .on('finish', () => resolve({ filename }))
      .on('error', reject)
  )
}

const unZipFile = async ({ source, target }) => {
  try {
    await extract(source, { dir: target })
  } catch (err) {
    // handle any errors
  }
}


export default {
  storeUpload,
  unZipFile
}
