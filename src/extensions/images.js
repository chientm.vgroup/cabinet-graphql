const fs = require('fs')
const sharp = require('sharp')
import mime from 'mime'

export const resizeImage = ({ path, width, height }) => {
  try {
    const readStream = fs.createReadStream(path)
    const format = mime.getExtension(mime.getType(path))
    if (['png', 'jpg', 'jpeg'].includes(format?.toLowerCase())) {
      let transform = sharp()
      if (format) {
        transform = transform.toFormat(format)
      }
      if (width || height) {
        transform = transform.resize(width, height, {
          fit: sharp.fit.inside,
          withoutEnlargement: true
        })
      }

      return readStream.pipe(transform)
    }
    throw new Error('Invalid file')
  } catch (error) {
    throw new Error(error)
  }
}
