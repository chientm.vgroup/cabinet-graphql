import { isObject, omitBy, isNil, isNull, isNaN, isEmpty } from 'lodash'
import stringExtensions from './string'

const checkIsObject = (object) =>
  !!(isObject(object) && Object.keys(object).length > 0)

const checkIsFalseyValue = (value) => {
  return !isNil(value) && !isNull(value) && !isNaN(value)
}

export const convertPropObjectToCapitalize = (object, propName = '') => {
  if (!checkIsObject(object)) {
    return object
  }

  const newObject = {}
  Object.keys(object).map((key) => {
    const capitalizeKey = stringExtensions.convertStringToCapitalize(key)
    const schemaValue = convertPropObjectToCapitalize(
      object[key],
      capitalizeKey
    )
    const internalValue = checkIsObject(schemaValue)
      ? omitBy(schemaValue, (value) => !checkIsFalseyValue(value))
      : schemaValue
      
    newObject[capitalizeKey] = internalValue
  })

  return omitBy(newObject, (value) => !checkIsFalseyValue(value))
}

export default {
  convertPropObjectToCapitalize
}
