-- 2020/07/07
ALTER TABLE `sql12352080`.`Category` 
ADD COLUMN `order` INT NULL DEFAULT 0 AFTER `parentId`;

ALTER TABLE `sql12352080`.`Product` 
ADD COLUMN `order` INT NULL DEFAULT 0 AFTER `updatedAt`;

ALTER TABLE `sql12352080`.`Property` 
ADD COLUMN `order` INT NULL DEFAULT 0 AFTER `updatedAt`;

-- 2020/08/22
ALTER TABLE `cabinet`.`Feature` 
ADD COLUMN `isPublished` TINYINT(1) NULL DEFAULT 1 AFTER `images`;

ALTER TABLE `cabinet`.`FeatureCategory` 
ADD COLUMN `isPublished` TINYINT(1) NULL DEFAULT 1 AFTER `description`;

ALTER TABLE `cabinet`.`FeatureProduct` 
ADD COLUMN `isPublished` TINYINT(1) NULL DEFAULT 1 AFTER `featureId`;

-- 2020/09/05
ALTER TABLE `cabinet`.`Property` 
ADD COLUMN `variables` JSON NULL AFTER `order`;
ALTER TABLE `cabinet`.`Property`
ADD COLUMN `propertyDefaultId` CHAR(56) NULL AFTER `productId`;

-- 2020/09/31
ALTER TABLE `cabinet`.`Property` 
CHANGE COLUMN `variables` `variables` TEXT NULL DEFAULT NULL ;

-- 2020/10/12
ALTER TABLE `cabinet`.`User` 
ADD COLUMN `password` VARCHAR(255) NOT NULL AFTER `email`;

-- 2020/11/06
ALTER TABLE `cabinet`.`FinishListProduct` 
ADD COLUMN `order` VARCHAR(45) NULL DEFAULT 0 AFTER `finishListId`;

-- 2020/11/25
ALTER TABLE `cabinet`.`UserSession` 
ADD COLUMN `activeStyle` CHAR(36) NULL AFTER `finishListProductId`,
ADD COLUMN `isCollageStyle` TINYINT(1) NULL DEFAULT 0 AFTER `activeStyle`;
